package org.example.uhy.openapi.controller;

import org.example.uhy.application.dto.MemberInfo;
import org.example.uhy.application.dto.MemberQueryCondition;
import org.example.uhy.application.dto.RegisterMemberDto;
import org.example.uhy.application.service.MemberService;
import org.example.uhy.domain.model.member.MemberId;
import org.example.uhy.openapi.dto.ResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 会员Rest Api
 */
@RestController
public class MemberController extends BaseController {
    @Autowired
    MemberService memberService;

    @GetMapping("/members")
    public ResponseDto membersOfLabel(@RequestParam(value = "label", required = false) String label, @RequestParam(value = "level", required = false) String level, HttpServletRequest request) {
        MemberQueryCondition condition = new MemberQueryCondition();
        condition.setLabel(label);
        condition.setLevel(level);
        List<MemberInfo> members = memberService.members(condition, createServiceContext(request));
        return ResponseDto.ok().withData(members);
    }

    @GetMapping("/member/{id}")
    public ResponseDto memberOfId(@PathVariable("id") String memberId, HttpServletRequest request) {
        MemberInfo member = memberService.memberOfId(memberId, createServiceContext(request));
        return ResponseDto.ok().withData(member);
    }

    /**
     * 新增会员
     *
     * @param memberDto 这里最好使用DTO，领域模型最好不对外暴露
     * @return
     */
    @PostMapping("/member/add")
    public ResponseDto addMember(@RequestBody RegisterMemberDto memberDto, HttpServletRequest request) {
        MemberId memberId = memberService.register(memberDto, createServiceContext(request));
        return ResponseDto.ok().withData(memberId.getId());
    }

    @PostMapping("/member/{id}/mark")
    public ResponseDto markMember(@PathVariable("id") String memberId, @RequestParam("label") String label, HttpServletRequest request) {
        memberService.mark(memberId, label, createServiceContext(request));
        return ResponseDto.ok();
    }
}