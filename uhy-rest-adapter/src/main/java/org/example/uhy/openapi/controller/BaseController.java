package org.example.uhy.openapi.controller;

import org.example.uhy.application.context.ServiceContext;

import javax.servlet.http.HttpServletRequest;

public class BaseController {
    private static final String ACCESS_TOKEN_KEY = "access-token";

    protected ServiceContext createServiceContext(HttpServletRequest request) {
        String token = request.getHeader(ACCESS_TOKEN_KEY);
        // 设置上下文信息
        return new ServiceContext(token);
    }
}
