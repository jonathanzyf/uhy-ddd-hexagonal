package org.example.uhy.openapi.dto;

/**
 * Rest 返回结果
 */
public class ResponseDto {
    private String code;
    private String message;
    private Object data;

    public ResponseDto() {
    }

    public ResponseDto(String code) {
        this.code = code;
    }

    public ResponseDto(String code, String message) {
        this(code);
        this.message = message;
    }

    public static ResponseDto ok() {
        return new ResponseDto("200");
    }

    public static ResponseDto failed(String message) {
        return new ResponseDto("400", message);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public ResponseDto withData(Object data) {
        this.setData(data);
        return this;
    }
}
