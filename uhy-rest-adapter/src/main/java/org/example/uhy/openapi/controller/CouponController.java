package org.example.uhy.openapi.controller;

import org.example.uhy.application.context.ServiceContext;
import org.example.uhy.application.dto.CouponArchiveInfo;
import org.example.uhy.application.dto.CouponInfo;
import org.example.uhy.application.dto.CreateCouponArchiveDto;
import org.example.uhy.application.dto.MemberQueryCondition;
import org.example.uhy.application.service.CouponService;
import org.example.uhy.application.service.MemberService;
import org.example.uhy.openapi.dto.ResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 卡券Rest Api
 */
@RestController
public class CouponController extends BaseController {
    @Autowired
    CouponService couponService;
    @Autowired
    MemberService memberService;

    @PostMapping("/coupons/{archiveId}/give/group")
    public ResponseDto givingCoupons(@PathVariable("archiveId") String archiveId, @RequestParam(value = "label", required = false) String label, @RequestParam(value = "level", required = false) String level, HttpServletRequest request) {
        ServiceContext context = createServiceContext(request);
        MemberQueryCondition condition = new MemberQueryCondition();
        condition.setLevel(level);
        condition.setLabel(label);
        String operatorId = context.currentOperator();
        List<String> couponIds = couponService.giveCouponsByCondition(archiveId, operatorId, condition, context);
        return ResponseDto.ok().withData(couponIds);
    }

    /**
     * 给会员送卡券
     *
     * @param archiveId
     * @param ownerIds
     * @param request
     * @return
     */
    @PostMapping("/coupons/{archiveId}/give")
    public ResponseDto givingCoupons(@PathVariable("archiveId") String archiveId, @RequestBody List<String> ownerIds, HttpServletRequest request) {
        ServiceContext context = createServiceContext(request);
        String operatorId = context.currentOperator();
        boolean isFans = "fans".equals(request.getParameter("is_member"));
        List<String> couponIds = null;
        if (isFans) {
            couponIds = couponService.giveCouponsToFans(archiveId, operatorId, ownerIds, context);
        } else {
            couponIds = couponService.giveCouponsToMembers(archiveId, operatorId, ownerIds, context);
        }
        return ResponseDto.ok().withData(couponIds);
    }

    @PostMapping("/coupon/{archiveId}/receive")
    public ResponseDto receiveCoupon(@PathVariable("archiveId") String archiveId, @RequestParam("activity") String activityId, HttpServletRequest request) {
        ServiceContext context = createServiceContext(request);
        String ownerId = context.currentOwner();
        boolean isFans = "fans".equals(request.getParameter("is_member"));
        String couponId = null;
        if (isFans) {
            couponId = couponService.receiveCouponByFans(archiveId, activityId, ownerId, context);
        } else {
            couponId = couponService.receiveCouponByMember(archiveId, activityId, ownerId, context);
        }
        return ResponseDto.ok().withData(couponId);
    }

    @RequestMapping("/coupon/{couponId}/consume")
    public ResponseDto consumeCoupon(@PathVariable("couponId") String couponId, HttpServletRequest request) {
        ServiceContext context = createServiceContext(request);
        String ownerId = context.currentOwner();
        couponService.consumeCoupon(couponId, ownerId, context);
        return ResponseDto.ok();
    }

    @GetMapping("/coupon/{id}")
    public ResponseDto findCoupon(@PathVariable("id") String id, HttpServletRequest request) {
        ServiceContext context = createServiceContext(request);
        CouponInfo couponInfo = couponService.couponOfId(id, context);
        return ResponseDto.ok().withData(couponInfo);
    }

    @PostMapping("/coupon/define/add")
    public ResponseDto addCouponArchive(@RequestBody CreateCouponArchiveDto archiveDto, HttpServletRequest request) {
        ServiceContext context = createServiceContext(request);
        String operatorId = context.currentOperator();
        String archiveId = couponService.addCouponArchive(archiveDto, operatorId, context);
        return ResponseDto.ok().withData(archiveId);
    }

    @GetMapping("/coupon/define/{id}")
    public ResponseDto addCouponArchive(@PathVariable("id") String id, HttpServletRequest request) {
        ServiceContext context = createServiceContext(request);
        CouponArchiveInfo archiveInfo = couponService.couponArchiveOfId(id, context);
        return ResponseDto.ok().withData(archiveInfo);
    }
}