package org.example.uhy.repository.impl;

import org.example.uhy.domain.model.member.MemberLevel;
import org.example.uhy.domain.model.member.MemberLevelId;
import org.example.uhy.domain.model.member.MemberLevelRepository;
import org.example.uhy.repository.dao.MemberLevelDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * 会员仓库实现类
 * 注意，这里是 {@link org.springframework.stereotype.Repository}
 */
@Repository
public class MemberLevelRepositoryImpl implements MemberLevelRepository {
    @Autowired
    MemberLevelDao memberLevelDao;

    @Override
    public MemberLevel defaultMemberLevel() {
        return memberLevelDao.defaultMemberLevel();
    }

    @Override
    public Optional<MemberLevel> memberLevelOfId(MemberLevelId levelId) {
        MemberLevel lvl = memberLevelDao.find(levelId);
        return Optional.ofNullable(lvl);
    }
}
