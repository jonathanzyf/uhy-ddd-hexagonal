package org.example.uhy.repository.dao;

import org.apache.ibatis.annotations.*;
import org.example.uhy.domain.model.member.Member;
import org.example.uhy.domain.model.member.MemberId;
import org.example.uhy.domain.model.member.MemberLabel;
import org.example.uhy.domain.model.member.MemberLevelId;

import java.util.List;

@Mapper
public interface MemberDao {
    @Select("select * from mm_member where id=#{id}")
    Member find(MemberId memberId);

    @Select("select * from mm_member where label=#{label}")
    List<Member> membersOfLabel(MemberLabel label);

    @Select("select * from mm_member where level_id=#{id}")
    List<Member> membersOfLevel(MemberLevelId levelId);

    @Select("select * from mm_member where label=#{label.label} and level_id=#{levelId.id}")
    List<Member> members(@Param("label") MemberLabel label, @Param("levelId") MemberLevelId levelId);

    @Insert("insert into mm_member(id,name,level_id,label,phone,create_time) values(#{id},#{name},#{levelId},#{label},#{phone},#{createTime})")
    void add(Member member);

    @Update("update mm_member set name=#{name},level_id=#{levelId},label=#{label},phone=#{phone},upgrade_time=#{upgradeTime},downgrade_time=#{downgradeTime} where id=#{id}")
    void save(Member member);
}