package org.example.uhy.repository.impl;

import org.example.uhy.domain.model.member.*;
import org.example.uhy.repository.dao.MemberDao;
import org.example.uhy.supports.mechanism.IdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * 会员仓库实现类
 * 注意，这里是 {@link org.springframework.stereotype.Repository}
 */
@Repository
public class MemberRepositoryImpl implements MemberRepository {
    @Autowired
    MemberDao memberDao;
    @Autowired
    IdGenerator idGenerator;

    @Override
    public Optional<Member> memberOfId(MemberId memberId) {
        Member member = memberDao.find(memberId);
        return Optional.ofNullable(member);
    }

    @Override
    public List<Member> membersOfLabel(MemberLabel label) {
        return memberDao.membersOfLabel(label);
    }

    @Override
    public List<Member> membersOfLevel(MemberLevelId levelId) {
        return memberDao.membersOfLevel(levelId);
    }

    @Override
    public List<Member> members(MemberLabel label, MemberLevelId levelId) {
        return memberDao.members(label, levelId);
    }

    @Override
    public void add(Member member) {
        memberDao.add(member);
    }

    @Override
    public void save(Member member) {
        memberDao.save(member);
    }

    @Override
    public MemberId nextIdentity() {
        return MemberId.of(idGenerator.nextIdentity());
    }
}
