package org.example.uhy.repository.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.example.uhy.domain.model.member.MemberLevel;
import org.example.uhy.domain.model.member.MemberLevelId;

@Mapper
public interface MemberLevelDao {
    @Select("select * from mm_member_level where id=#{id}")
    MemberLevel find(MemberLevelId levelId);

    @Select("select * from mm_member_level order by level asc limit 0,1")
    MemberLevel defaultMemberLevel();
}
