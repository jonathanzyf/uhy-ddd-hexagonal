package org.example.uhy.repository.impl;

import org.example.uhy.domain.model.coupon.CouponArchive;
import org.example.uhy.domain.model.coupon.CouponArchiveId;
import org.example.uhy.domain.model.coupon.CouponArchiveRepository;
import org.example.uhy.repository.dao.CouponArchiveDao;
import org.example.uhy.supports.mechanism.IdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * 卡券定义档案仓库实现类
 * 注意，这里是 {@link org.springframework.stereotype.Repository}
 */
@Repository
public class CouponArchiveRepositoryImpl implements CouponArchiveRepository {
    @Autowired
    CouponArchiveDao couponArchiveDao;
    @Autowired
    IdGenerator idGenerator;

    @Override
    public void add(CouponArchive definition) {
        couponArchiveDao.add(definition);
    }

    @Override
    public Optional<CouponArchive> couponArchiveOfId(CouponArchiveId couponArchiveId) {
        CouponArchive archive = couponArchiveDao.find(couponArchiveId);
        return Optional.ofNullable(archive);
    }

    @Override
    public void save(CouponArchive couponArchive) {
        couponArchiveDao.save(couponArchive);
    }

    @Override
    public CouponArchiveId nextIdentity() {
        return CouponArchiveId.of(idGenerator.nextIdentity());
    }
}
