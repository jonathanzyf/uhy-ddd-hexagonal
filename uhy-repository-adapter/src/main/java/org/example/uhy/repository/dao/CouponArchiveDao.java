package org.example.uhy.repository.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.example.uhy.domain.model.coupon.CouponArchive;
import org.example.uhy.domain.model.coupon.CouponArchiveId;

/**
 * 卡券定义档案数据访问对象
 */
@Mapper
public interface CouponArchiveDao {
    @Select("select id,name,description,effective_from_time as `effectiveTime.fromTime`,effective_to_time as `effectiveTime.toTime`,planned_quantity as `stock.plannedQuantity`,quantity as `stock.quantity`,scope as `constraint.scope`,count_per_owner as `constraint.countPerOwner`,type as `parameter.type`,quota as `parameter.quota`,decrease as `parameter.decrease`,counting as `parameter.counting`,discount as `parameter.discount` from mm_coupon_archive where id=#{id}")
    CouponArchive find(CouponArchiveId couponArchiveId);

    @Insert("insert into mm_coupon_archive(id,name,description,effective_from_time,effective_to_time,planned_quantity,quantity,scope,count_per_owner,type,quota,decrease,counting,discount,create_time,create_user) values(#{id},#{name},#{description},#{effectiveTime.fromTime},#{effectiveTime.toTime},#{stock.plannedQuantity},#{stock.quantity},#{constraint.scope},#{constraint.countPerOwner},#{parameter.type},#{parameter.quota},#{parameter.decrease},#{parameter.counting},#{parameter.discount},#{createTime},#{createUser})")
    void add(CouponArchive definition);

    /**
     * 更新现存量
     * 考虑乐观锁，防止并发访问时超发卡券问题
     *
     * @param archive
     */
    @Update("update mm_coupon_archive set quantity=#{stock.quantity} where id=#{id}")
    void save(CouponArchive archive);
}
