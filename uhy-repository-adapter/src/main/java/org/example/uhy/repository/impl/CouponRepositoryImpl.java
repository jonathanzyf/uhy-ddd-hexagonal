package org.example.uhy.repository.impl;

import org.example.uhy.domain.model.coupon.*;
import org.example.uhy.repository.dao.CouponDao;
import org.example.uhy.supports.mechanism.IdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * 卡券仓库实现类
 * 注意，这里是 {@link org.springframework.stereotype.Repository}
 */
@Repository
public class CouponRepositoryImpl implements CouponRepository {
    @Autowired
    CouponDao couponDao;
    @Autowired
    IdGenerator idGenerator;

    @Override
    public Integer countOfOwner(CouponArchiveId archiveId, CouponOwner owner) {
        if (owner.isMember()) {
            return couponDao.countOfMember(archiveId, owner.getOwnerId());
        } else {
            return couponDao.countOfFans(archiveId, owner.getOwnerId());
        }
    }

    @Override
    public Optional<Coupon> couponOfId(CouponId couponId) {
        Coupon coupon = couponDao.find(couponId);
        return Optional.ofNullable(coupon);
    }

    @Override
    public void add(Coupon coupon) {
        couponDao.add(coupon);
    }

    @Override
    public void save(Coupon coupon) {
        couponDao.save(coupon);
    }

    @Override
    public List<Coupon> scanExpiredCoupons() {
        return couponDao.allOutOfDateCoupons(LocalDateTime.now());
    }

    @Override
    public CouponId nextIdentity() {
        return CouponId.of(idGenerator.nextIdentity());
    }
}
