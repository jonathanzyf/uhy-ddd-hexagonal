package org.example.uhy.repository.config;

import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({EnableOrmAutoConfigRegistrar.class})
public @interface EnableOrmAutoConfig {
    String daoBasePackage() default "";

    @AliasFor("dataSourceBeanName")
    String value() default "dataSource";

    @AliasFor("value")
    String dataSourceBeanName() default "dataSource";
}
