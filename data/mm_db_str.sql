create table mm_coupon_archive(
    id varchar(40) primary key,
    name varchar(50) not null,
    description varchar(255),
    effective_from_time datetime not null,
    effective_to_time datetime not null,
    type varchar(20) not null,
    quota decimal(12,2),
    decrease decimal(12,2),
    discount decimal(2,2),
    counting int,
    planned_quantity int not null,
    quantity int not null,
    scope varchar(50),
    count_per_owner int,
    create_time datetime not null,
    create_user varchar(40) not null
);

create table mm_coupon (
    id varchar(40) primary key,
    archive_id varchar(40) not null,
    name varchar(50) not null,
    effective_from_time datetime not null,
    effective_to_time datetime not null,
    type varchar(20) not null,
    quota decimal(12,2),
    decrease decimal(12,2),
    discount decimal(2,2),
    counting int,
    source_id varchar(40),
    source_type varchar(20),
    status int not null,
    operator_id varchar(40),
    member_id varchar(40),
    fans_id varchar(40),
    owner_type char(1),
    create_time datetime not null,
    send_time datetime,
    receive_time datetime,
    consume_time datetime,
    cancel_time datetime,
    back_time datetime
);

create table mm_member(
    id varchar(40) primary key,
    name varchar(50) not null,
    phone varchar(20) not null,
    level_id varchar(40) not null,
    label varchar(40),
    create_time datetime not null,
    upgrade_time datetime,
    downgrade_time datetime
);

create table mm_member_level(
    id varchar(40) primary key,
    name varchar(50) not null,
    level int not null
);