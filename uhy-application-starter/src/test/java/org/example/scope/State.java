package org.example.scope;

public class State {
    Container container;

    public State(Container container) {
        this.container = container;
    }

    public void changeName(String newName) {
        container.name = newName;
    }
}
