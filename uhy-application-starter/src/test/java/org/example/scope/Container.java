package org.example.scope;

public class Container {
    protected String name;

    public String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }
}
