package org.example.scope;

public class ScopeTest {
    public static void main(String[] args) {
        Container container = new Container();

        State state = new State(container);
        state.changeName("hello");
        System.out.println(container.getName());
    }
}
