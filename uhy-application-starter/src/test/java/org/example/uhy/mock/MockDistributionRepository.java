package org.example.uhy.mock;

import org.example.uhy.domain.model.distribution.DistributionRepository;
import org.example.uhy.domain.model.distribution.DistributorId;
import org.example.uhy.domain.model.member.MemberId;
import org.example.uhy.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class MockDistributionRepository implements DistributionRepository {
    private Integer membersCount;

    public MockDistributionRepository(Integer membersCount) {
        this.membersCount = membersCount;
    }

    @Override
    public Integer membersCountOfDistributor(DistributorId distributorId) {
        return membersCount;
    }

    @Override
    public List<MemberId> membersOfDistributor(DistributorId distributorId) {
        List<MemberId> memberIds = new ArrayList<>();
        for (int i = 1; i <= membersCount; i++) {
            String no = StringUtils.leftPadding(String.valueOf(i), 3, '0');
            memberIds.add(MemberId.of("member-id-" + no));
        }
        return memberIds;
    }
}
