package org.example.uhy.mock;

import org.example.uhy.domain.model.member.MemberId;
import org.example.uhy.domain.model.trade.Trade;
import org.example.uhy.domain.model.trade.TradeId;
import org.example.uhy.domain.model.trade.TradeRepository;

import java.math.BigDecimal;
import java.util.List;

public class MockTradeRepository implements TradeRepository {
    private BigDecimal moneyPerMember;
    private Integer countPerMember;

    public MockTradeRepository(BigDecimal moneyPerMember, Integer countPerMember) {
        this.moneyPerMember = moneyPerMember;
        this.countPerMember = countPerMember;
    }

    @Override
    public Trade tradeOfId(TradeId tradeId) {
        return null;
    }

    @Override
    public BigDecimal accumulationMoneyOfMember(MemberId memberId) {
        return moneyPerMember;
    }

    @Override
    public BigDecimal accumulationMoneyOfMembers(List<MemberId> memberIds) {
        return moneyPerMember.multiply(BigDecimal.valueOf(memberIds.size()));
    }

    @Override
    public Integer accumulationCountOfMember(MemberId memberId) {
        return countPerMember;
    }

    @Override
    public Integer accumulationCountOfMembers(List<MemberId> memberIds) {
        return countPerMember * memberIds.size();
    }
}
