package org.example.uhy.coupon;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.uhy.UhyApplication;
import org.example.uhy.domain.model.coupon.CouponStatus;
import org.example.uhy.loader.TestDataLoader;
import org.example.uhy.openapi.dto.ResponseDto;
import org.example.uhy.rest.RestClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {UhyApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class CouponTest {
    private static final String test_access_token = "a-access-token";
    protected MockMvc mvc;
    @Autowired
    WebApplicationContext webApplicationContext;
    @Autowired
    ObjectMapper objectMapper;

    @Before
    public void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    /**
     * 新增卡券定义
     */
    @Test
    public void test01_add_coupon_archive() {
        // 创建卡券定义
        String id = create_an_coupon_archive();
        // 查看卡券定义
        find_coupon_archive(id, 1000);
    }

    /**
     * 收取卡券
     */
    @Test
    public void test03_receive_coupon() {
        // 创建卡券定义
        String archiveId = create_an_coupon_archive();
        // 领取卡券
        ResponseDto responseDto = receive_a_coupon(archiveId);
        assertEquals("200", responseDto.getCode());
        assertTrue(responseDto.getData() instanceof String);
        String couponId = (String) responseDto.getData();
        // 查看卡券
        find_coupon(couponId);
        // 卡券定义可用数量
        find_coupon_archive(archiveId, 999);
        // 消费卡券
        consume_coupon(couponId);
        // 查看卡券状态
        ResponseDto resp2 = find_coupon(couponId);
        assertEquals(CouponStatus.Consumed.ordinal(), ((Map<String, Object>) resp2.getData()).get("status"));
        // 再次发放卡券，卡券资格检查不通过
        ResponseDto resp3 = receive_a_coupon(archiveId);
        assertEquals("超出卡券领用限制，每人最多领用1张", resp3.getMessage());
    }

    /**
     * 根据条件分发卡券
     */
    @Test
    public void test04_give_coupon_group() {
        // 创建卡券定义
        String archiveId = create_an_coupon_archive();
        // 按条件发放卡券
        String body = "label=" + "高富帅" + "&level=" + "Level-one";
        String uri = "/coupons/" + archiveId + "/give/group";
        RestClient client = new RestClient(mvc, objectMapper, test_access_token);
        ResponseDto responseDto = client.formPost(uri, body);
        assertEquals("200", responseDto.getCode());
        assertTrue(responseDto.getData() instanceof List);
        int count = ((List) responseDto.getData()).size();
        assertTrue(count > 0);
        // 卡券定义可用数量
        find_coupon_archive(archiveId, 1000 - count);
    }

    private String create_an_coupon_archive() {
        String body = new TestDataLoader(objectMapper).load("classpath:coupon-archive.json");
        String uri = "/coupon/define/add";
        RestClient client = new RestClient(mvc, objectMapper, test_access_token);
        ResponseDto responseDto = client.post(uri, body);
        assertEquals("200", responseDto.getCode());
        assertTrue(responseDto.getData() instanceof String);
        return (String) responseDto.getData();
    }


    private void consume_coupon(String couponId) {
        String uri = "/coupon/" + couponId + "/consume";
        RestClient client = new RestClient(mvc, objectMapper, test_access_token);
        ResponseDto responseDto = client.get(uri);
        assertEquals("200", responseDto.getCode());
    }


    private ResponseDto find_coupon(String couponId) {
        String uri = "/coupon/" + couponId;
        RestClient client = new RestClient(mvc, objectMapper, test_access_token);
        ResponseDto responseDto = client.get(uri);
        assertEquals("200", responseDto.getCode());
        Map<String, Object> data = (Map<String, Object>) responseDto.getData();
        assertEquals("快乐吃卡券", data.get("name"));
        assertEquals(100.0, data.get("quota"));
        assertEquals(30.0, data.get("decrease"));
        return responseDto;
    }

    private void find_coupon_archive(String id, Integer currentQuantity) {
        String uri = "/coupon/define/" + id;
        RestClient client = new RestClient(mvc, objectMapper, test_access_token);
        ResponseDto responseDto = client.get(uri);
        assertEquals("200", responseDto.getCode());
        assertTrue(responseDto.getData() instanceof Map);
        Map<String, Object> data = (Map<String, Object>) responseDto.getData();
        assertEquals("快乐吃卡券", data.get("name"));
        assertEquals(1000, data.get("plannedQuantity"));
        assertEquals(currentQuantity, data.get("quantity"));
        assertEquals(100.0, data.get("quota"));
        assertEquals(30.0, data.get("decrease"));
    }

    private ResponseDto receive_a_coupon(String archiveId) {
        final String activityId = "an-activity-id";
        String body = "activity=" + activityId;
        String uri = "/coupon/" + archiveId + "/receive";
        RestClient client = new RestClient(mvc, objectMapper, test_access_token);
        ResponseDto responseDto = client.formPost(uri, body);
        return responseDto;
    }
}