package org.example.uhy.upgrade;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.uhy.domain.model.distribution.DistributionRepository;
import org.example.uhy.domain.model.distribution.DistributorId;
import org.example.uhy.domain.model.trade.TradeRepository;
import org.example.uhy.domain.model.upgrade.UpgradeService;
import org.example.uhy.domain.model.upgrade.execution.UpgradeExecutionContext;
import org.example.uhy.domain.model.upgrade.execution.UpgradeObject;
import org.example.uhy.domain.model.upgrade.schema.UpgradeSchema;
import org.example.uhy.mock.MockDistributionRepository;
import org.example.uhy.mock.MockTradeRepository;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class UpgradeTest {
    @Test
    public void test01_upgrade() throws IOException {
        UpgradeSchema schema = loadUpgradeSchema();
        /*
        晋级方案执行树
        AND
        ├──OR
        │   ├──MEMBER_COUNT	20
        │   ├──TRADE_COUNT	40
        ├──TRADE_SUM	10000
         */
        UpgradeService upgradeService = new UpgradeService();
        UpgradeExecutionContext executionContext;
        boolean canUpgrade;
        // 20 满足 200 满足 10000 满足
        executionContext = createExecutionContext(BigDecimal.valueOf(500), 10, 20);
        canUpgrade = upgradeService.upgrade(schema, executionContext);
        assertTrue(canUpgrade);
        // 20 满足 200 满足 8000 不满足
        executionContext = createExecutionContext(BigDecimal.valueOf(400), 10, 20);
        canUpgrade = upgradeService.upgrade(schema, executionContext);
        assertFalse(canUpgrade);
        // 20 满足 20 不满足 10000 满足
        executionContext = createExecutionContext(BigDecimal.valueOf(500), 1, 20);
        canUpgrade = upgradeService.upgrade(schema, executionContext);
        assertTrue(canUpgrade);
        // 20 满足 20 不满足 8000 不满足
        executionContext = createExecutionContext(BigDecimal.valueOf(400), 1, 20);
        canUpgrade = upgradeService.upgrade(schema, executionContext);
        assertFalse(canUpgrade);
        // 10 不满足 100 满足 10000 满足
        executionContext = createExecutionContext(BigDecimal.valueOf(1000), 10, 10);
        canUpgrade = upgradeService.upgrade(schema, executionContext);
        assertTrue(canUpgrade);
        // 10 不满足 100 满足 8000 不满足
        executionContext = createExecutionContext(BigDecimal.valueOf(800), 10, 10);
        canUpgrade = upgradeService.upgrade(schema, executionContext);
        assertFalse(canUpgrade);
        // 10 不满足 30 不满足 10000 满足
        executionContext = createExecutionContext(BigDecimal.valueOf(1000), 3, 10);
        canUpgrade = upgradeService.upgrade(schema, executionContext);
        assertFalse(canUpgrade);
        // 10 不满足 30 不满足 8000 不满足
        executionContext = createExecutionContext(BigDecimal.valueOf(800), 3, 10);
        canUpgrade = upgradeService.upgrade(schema, executionContext);
        assertFalse(canUpgrade);
    }

    private UpgradeExecutionContext createExecutionContext(BigDecimal moneyPerMember, Integer countPerMember, Integer membersCount) {
        TradeRepository tradeRepository = new MockTradeRepository(moneyPerMember, countPerMember);
        DistributionRepository distributionRepository = new MockDistributionRepository(membersCount);

        UpgradeObject upgradeObject = UpgradeObject.of(DistributorId.of("10001L"));
        return new UpgradeExecutionContext(upgradeObject, tradeRepository, distributionRepository);
    }

    private UpgradeSchema loadUpgradeSchema() throws IOException {
        InputStream is = UpgradeTest.class.getClassLoader().getResourceAsStream("upgrade-schema.json");
        ObjectMapper objectMapper = new ObjectMapper();
        UpgradeSchema schema = objectMapper.readValue(is, UpgradeSchema.class);
        return schema;
    }
}
