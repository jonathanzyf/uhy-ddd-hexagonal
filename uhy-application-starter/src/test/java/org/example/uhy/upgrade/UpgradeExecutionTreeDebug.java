package org.example.uhy.upgrade;

import org.example.uhy.domain.model.upgrade.execution.builder.UpgradeExecutionNodeFactory;
import org.example.uhy.domain.model.upgrade.execution.builder.UpgradeSchemaParser;
import org.example.uhy.domain.model.upgrade.execution.UpgradeExecutionTree;
import org.example.uhy.domain.model.upgrade.execution.node.ConditionUpgradeExecutionNode;
import org.example.uhy.domain.model.upgrade.execution.node.LogicalExecutionNode;
import org.example.uhy.domain.model.upgrade.execution.node.UpgradeExecutionNode;
import org.example.uhy.domain.model.upgrade.schema.UpgradeSchema;

import java.util.Iterator;

public class UpgradeExecutionTreeDebug {
    public void debug(UpgradeSchema schema) {
        UpgradeExecutionTree tree = new UpgradeSchemaParser().parse(schema, UpgradeExecutionNodeFactory.instance());
        UpgradeExecutionNode root = tree.getRoot();
        print(root, "");
    }

    private void print(UpgradeExecutionNode node, String prefix) {
        if (node instanceof LogicalExecutionNode) {
            LogicalExecutionNode logicalNode = (LogicalExecutionNode) node;
            System.out.println(prefix + logicalNode.toString());
            Iterator<UpgradeExecutionNode> it = logicalNode.iterator();
            while (it.hasNext()) {
                print(it.next(), prefix.replace("├──", "│   ") + "├──");
            }
        } else {
            ConditionUpgradeExecutionNode conditionNode = (ConditionUpgradeExecutionNode) node;
            System.out.println(prefix + conditionNode.toString());
        }
    }
}
