package org.example.uhy.member;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.uhy.UhyApplication;
import org.example.uhy.loader.TestDataLoader;
import org.example.uhy.openapi.dto.ResponseDto;
import org.example.uhy.rest.RestClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {UhyApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class MemberTest {
    private static final String test_access_token = "a-access-token";
    protected MockMvc mvc;
    @Autowired
    WebApplicationContext webApplicationContext;
    @Autowired
    ObjectMapper objectMapper;

    @Before
    public void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    /**
     * 新增会员
     */
    @Test
    public void test01_create_member() {
        String body = new TestDataLoader(objectMapper).load("classpath:member.json");
        String uri = "/member/add";
        RestClient client = new RestClient(mvc, objectMapper, test_access_token);
        ResponseDto responseDto = client.post(uri, body);
        assertEquals("200", responseDto.getCode());
        System.out.println("member id is :" + responseDto.getData());
    }

    /**
     * 根据ID查询会员
     */
    @Test
    public void test02_find_member() {
        // @GetMapping("/member/{id}")
        String uri = "/member/" + "a-member-id";
        RestClient client = new RestClient(mvc, objectMapper, test_access_token);
        ResponseDto responseDto = client.get(uri);
        assertEquals("200", responseDto.getCode());
        assertNotNull(responseDto.getData());
        assertTrue(responseDto.getData() instanceof Map);
        assertEquals("张三", ((Map) responseDto.getData()).get("name"));
        System.out.println(responseDto.getData());
    }

    /**
     * 给会员打标签
     */
    @Test
    public void test03_mark_member() {
        String uri = "/member/" + "a-member-id" + "/mark";
        String body = "label=" + "高富帅";
        RestClient client = new RestClient(mvc, objectMapper, test_access_token);
        ResponseDto responseDto = client.formPost(uri, body);
        assertEquals("200", responseDto.getCode());

        responseDto = client.get("/member/" + "a-member-id");
        assertEquals("200", responseDto.getCode());
        assertEquals("高富帅", ((Map) responseDto.getData()).get("label"));
        System.out.println(responseDto.getData());
    }

    /**
     * 根据条件查询会员
     */
    @Test
    public void test04_query_members() {
        String uri = "/members?label=" + "高富帅" + "&level=" + "level-one";
        RestClient client = new RestClient(mvc, objectMapper, test_access_token);
        ResponseDto responseDto = client.get(uri);
        assertEquals("200", responseDto.getCode());
        assertNotNull(responseDto.getData());
        assertTrue(responseDto.getData() instanceof List);
        assertEquals(1, ((List) responseDto.getData()).size());
        System.out.println(responseDto.getData());
    }
}
