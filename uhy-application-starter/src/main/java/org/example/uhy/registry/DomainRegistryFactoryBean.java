package org.example.uhy.registry;

import org.example.uhy.domain.mechanism.DomainEventPublisher;
import org.example.uhy.domain.registry.DomainRegistry;
import org.example.uhy.domain.model.coupon.CouponArchiveRepository;
import org.example.uhy.domain.model.coupon.CouponRepository;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.function.BiFunction;

public class DomainRegistryFactoryBean implements FactoryBean, ApplicationContextAware {
    ApplicationContext applicationContext;
    @Autowired
    DomainEventPublisher eventPublisher;
    @Autowired
    CouponArchiveRepository couponArchiveRepository;
    @Autowired
    CouponRepository couponRepository;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Override
    public DomainRegistry getObject() throws Exception {
        BiFunction<String, Class, Object> beanFinder = (beanName, clazz) -> applicationContext.getBean(beanName, clazz);
        return new DomainRegistry(couponArchiveRepository, couponRepository, eventPublisher, beanFinder);
    }

    @Override
    public Class<?> getObjectType() {
        return DomainRegistry.class;
    }
}