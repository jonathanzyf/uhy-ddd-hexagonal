package org.example.uhy.listener;

import org.example.uhy.domain.registry.DomainRegistry;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class UhyApplicationListener implements ApplicationListener<ContextRefreshedEvent> {
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        DomainRegistry registry = event.getApplicationContext().getBean(DomainRegistry.class);
    }
}
