package org.example.uhy.mechanism;

import org.example.uhy.supports.mechanism.IdGenerator;

import java.util.UUID;

public class UuidIdGenerator implements IdGenerator {
    @Override
    public String nextIdentity() {
        return UUID.randomUUID().toString();
    }
}
