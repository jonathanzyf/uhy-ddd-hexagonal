package org.example.uhy.exceptions;

/**
 * 参数异常
 */
public class IllegalParameterException extends RuntimeException {
    private static final long serialVersionUID = 2637527869552951324L;

    public IllegalParameterException(String message) {
        super(message);
    }

    public IllegalParameterException(String message, Throwable cause) {
        super(message, cause);
    }
}
