package org.example.uhy.domain.model.member;

import org.example.uhy.exceptions.IllegalParameterException;
import org.example.uhy.supports.meta.ValueObject;

/**
 * 会员ID
 */
@ValueObject
public class MemberId {
    private String id;

    private MemberId() {
    }

    private MemberId(String memberId) {
        this.setId(memberId);
    }

    public static MemberId of(String memberId) {
        return new MemberId(memberId);
    }

    public String getId() {
        return id;
    }

    private void setId(String memberId) {
        if (memberId == null || memberId.length() == 0) {
            throw new IllegalParameterException("请输入会员ID");
        }
        this.id = memberId;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MemberId) {
            return id.equals(((MemberId) obj).id);
        }
        return false;
    }
}
