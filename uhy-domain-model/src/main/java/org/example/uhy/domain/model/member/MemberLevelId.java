package org.example.uhy.domain.model.member;

import org.example.uhy.exceptions.IllegalParameterException;
import org.example.uhy.supports.meta.ValueObject;

/**
 * 会员级别ID
 */
@ValueObject
public class MemberLevelId {
    private String id;

    private MemberLevelId() {
    }

    private MemberLevelId(String levelId) {
        this.setId(levelId);
    }

    public static MemberLevelId of(String levelId) {
        return new MemberLevelId(levelId);
    }

    public String getId() {
        return id;
    }

    private void setId(String levelId) {
        if (levelId == null || levelId.length() == 0) {
            throw new IllegalParameterException("请输入会员级别ID");
        }
        this.id = levelId;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MemberLevelId) {
            return id.equals(((MemberLevelId) obj).id);
        }
        return false;
    }
}
