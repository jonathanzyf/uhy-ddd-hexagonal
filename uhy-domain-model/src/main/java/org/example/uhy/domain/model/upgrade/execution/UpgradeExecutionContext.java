package org.example.uhy.domain.model.upgrade.execution;

import org.example.uhy.domain.model.distribution.DistributionRepository;
import org.example.uhy.domain.model.trade.TradeRepository;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 晋级执行上下文
 */
public class UpgradeExecutionContext {
    private DistributionRepository distributionRepository;
    private TradeRepository tradeRepository;
    private UpgradeObject upgradeObject;
    private Map<String, Object> cachedValues = new ConcurrentHashMap<>();

    public UpgradeExecutionContext(UpgradeObject upgradeObject, TradeRepository tradeRepository, DistributionRepository distributionRepository) {
        this.setUpgradeObject(upgradeObject);
        this.tradeRepository(tradeRepository);
        this.distributionRepository(distributionRepository);
    }

    public boolean hasCachedValue(String key) {
        return cachedValues.containsKey(key);
    }

    public <T> T getCachedValue(String key) {
        return (T) cachedValues.get(key);
    }

    public void setCachedValue(String key, Object value) {
        cachedValues.put(key, value);
    }

    public UpgradeObject upgradeObject() {
        return upgradeObject;
    }

    private void setUpgradeObject(UpgradeObject upgradeObject) {
        if (upgradeObject == null) {
            throw new IllegalArgumentException("请输入晋级对象");
        }
        this.upgradeObject = upgradeObject;
    }

    public DistributionRepository distributionRepository() {
        return distributionRepository;
    }

    private void distributionRepository(DistributionRepository distributionRepository) {
        if (upgradeObject == null) {
            throw new IllegalArgumentException("请输入分销仓库对象");
        }
        this.distributionRepository = distributionRepository;
    }

    public TradeRepository tradeRepository() {
        return tradeRepository;
    }

    private void tradeRepository(TradeRepository tradeRepository) {
        if (upgradeObject == null) {
            throw new IllegalArgumentException("请输入交易仓库对象");
        }
        this.tradeRepository = tradeRepository;
    }
}
