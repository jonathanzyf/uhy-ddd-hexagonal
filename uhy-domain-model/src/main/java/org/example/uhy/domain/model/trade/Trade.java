package org.example.uhy.domain.model.trade;

import org.example.uhy.supports.meta.Aggregate;

/**
 * 交易
 */
@Aggregate
public class Trade {
    private TradeId tradeId;

    public Trade(TradeId tradeId) {
        this.tradeId = tradeId;
    }

    public TradeId tradeId() {
        return tradeId;
    }

    public void tradeId(TradeId tradeId) {
        this.tradeId = tradeId;
    }

    public String getId() {
        return tradeId.toString();
    }

    public void setId(String tradeId) {
        this.tradeId = TradeId.of(tradeId);
    }
}
