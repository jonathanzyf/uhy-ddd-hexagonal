package org.example.uhy.domain.model.coupon;

import org.example.uhy.domain.model.base.TimeRange;
import org.example.uhy.supports.meta.Aggregate;

import java.math.BigDecimal;

/**
 * 有消费额度限制的折扣卡券
 */
@Aggregate
public class QuotaDiscountCoupon extends DiscountCoupon {
    public static final String TYPE = "quota_discount"; // 有门槛的折扣券

    protected QuotaDiscountCoupon() {
        super();
    }

    public QuotaDiscountCoupon(CouponId couponId, CouponArchiveId archiveId, String name, TimeRange effectiveTime, CouponParameter parameter) {
        super(couponId, archiveId, name, effectiveTime, parameter);
    }

    public BigDecimal getQuota() {
        return parameter().getQuota();
    }

}
