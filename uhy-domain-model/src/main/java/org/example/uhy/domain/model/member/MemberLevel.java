package org.example.uhy.domain.model.member;

import org.example.uhy.exceptions.IllegalParameterException;
import org.example.uhy.supports.meta.Aggregate;

/**
 * 会员级别
 */
@Aggregate
public class MemberLevel {
    private MemberLevelId levelId;
    private String name;

    private MemberLevel() {
    }

    public MemberLevel(MemberLevelId levelId, String name) {
        this.levelId(levelId);
        this.setName(name);
    }

    public MemberLevelId levelId() {
        return levelId;
    }

    // for constructor
    private void levelId(MemberLevelId levelId) {
        if (levelId == null) {
            throw new IllegalParameterException("请输入会员级别ID");
        }
        this.levelId = levelId;
    }

    // for orm
    private String getId() {
        return levelId.getId();
    }

    // for orm
    private void setId(String levelId) {
        this.levelId = MemberLevelId.of(levelId);
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        if (name == null || name.length() == 0) {
            throw new IllegalParameterException("请输入会员姓名");
        }
        this.name = name;
    }
}
