package org.example.uhy.domain.model.coupon.event;

import org.example.uhy.domain.mechanism.DomainEvent;
import org.example.uhy.domain.model.coupon.CouponId;

/**
 * 卡券已废弃领域事件
 */
public class CouponCancelled extends DomainEvent {
    private CouponId couponId;

    public CouponCancelled(CouponId couponId) {
        this.couponId = couponId;
    }

    public String getCouponId() {
        return couponId.getId();
    }
}
