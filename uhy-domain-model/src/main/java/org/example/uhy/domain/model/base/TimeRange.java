package org.example.uhy.domain.model.base;

import org.example.uhy.exceptions.IllegalParameterException;
import org.example.uhy.supports.meta.ValueObject;

import java.time.LocalDateTime;

/**
 * 时间范围
 */
@ValueObject
public class TimeRange {
    private LocalDateTime fromTime;
    private LocalDateTime toTime;

    private TimeRange() {
    }

    private TimeRange(LocalDateTime fromTime, LocalDateTime toTime) {
        this.setFromTime(fromTime);
        this.setToTime(toTime);
        this.checkTime();
    }

    public static TimeRange of(LocalDateTime fromTime, LocalDateTime toTime) {
        return new TimeRange(fromTime, toTime);
    }

    public LocalDateTime getFromTime() {
        return fromTime;
    }

    private void setFromTime(LocalDateTime fromTime) {
        this.fromTime = fromTime;
    }

    public LocalDateTime getToTime() {
        return toTime;
    }

    private void setToTime(LocalDateTime toTime) {
        this.toTime = toTime;
    }

    private void checkTime() {
        if (this.getFromTime().isAfter(this.getToTime())) {
            throw new IllegalParameterException("开始时间不能大于结束时间");
        }
    }

    public boolean inRange(LocalDateTime ldt) {
        return ldt.isAfter(fromTime) && ldt.isBefore(toTime);
    }
}
