package org.example.uhy.domain.model.coupon.event;

import org.example.uhy.domain.mechanism.DomainEvent;
import org.example.uhy.domain.model.coupon.CouponId;

/**
 * 卡券已过期领域事件
 */
public class CouponExpired extends DomainEvent {
    private CouponId couponId;

    public CouponExpired(CouponId couponId) {
        this.couponId = couponId;
    }

    public String getCouponId() {
        return couponId.getId();
    }
}
