package org.example.uhy.domain.model.upgrade.schema;

import org.example.uhy.supports.meta.ValueObject;

/**
 * 规则类型
 */
@ValueObject
public class UpgradeRuleType {
    public static final UpgradeRuleType LOGICAL = new UpgradeRuleType("LOGICAL");

    private String ruleType;

    private UpgradeRuleType() {
    }

    private UpgradeRuleType(String ruleType) {
        this.ruleType = ruleType;
    }

    public static UpgradeRuleType of(String ruleType) {
        return new UpgradeRuleType(ruleType);
    }

    public String getRuleType() {
        return ruleType;
    }

    private void setRuleType(String ruleType) {
        this.ruleType = ruleType;
    }
}
