package org.example.uhy.domain.model.marketing;

import org.example.uhy.domain.mechanism.DomainEvent;
import org.example.uhy.domain.model.fans.FansId;
import org.example.uhy.domain.model.member.MemberId;

/**
 * 营销活动奖励事件
 */
@org.example.uhy.supports.meta.DomainEvent
public class ActivityAwarded extends DomainEvent {
    private ActivityId activityId;
    private MemberId memberId;
    private FansId fansId;

    private ActivityAwarded() {
    }

    public ActivityAwarded(ActivityId activityId, MemberId memberId) {
        this.activityId(activityId);
        this.memberId(memberId);
    }

    public ActivityAwarded(ActivityId activityId, FansId fansId) {
        this.activityId(activityId);
        this.fansId(fansId);
    }

    public ActivityId activityId() {
        return activityId;
    }

    private void activityId(ActivityId activityId) {
        if (activityId == null) {
            throw new IllegalArgumentException("请输入活动ID");
        }
        this.activityId = activityId;
    }

    private String getActivityId() {
        return activityId.getActivityId();
    }

    private void setActivityId(String activityId) {
        this.activityId = ActivityId.of(activityId);
    }

    public MemberId memberId() {
        return memberId;
    }

    private void memberId(MemberId memberId) {
        if (memberId == null) {
            throw new IllegalArgumentException("请输入会员ID");
        }
        this.memberId = memberId;
    }

    private String getMemberId() {
        return memberId == null ? null : memberId.getId();
    }

    private void setMemberId(String memberId) {
        this.memberId = MemberId.of(memberId);
    }

    public FansId fansId() {
        return fansId;
    }

    private void fansId(FansId fansId) {
        if (fansId == null) {
            throw new IllegalArgumentException("请输入粉丝ID");
        }
        this.fansId = fansId;
    }

    private String getFansId() {
        return fansId == null ? null : fansId.getFansId();
    }

    private void setFansId(String fansId) {
        this.fansId = FansId.of(fansId);
    }
}
