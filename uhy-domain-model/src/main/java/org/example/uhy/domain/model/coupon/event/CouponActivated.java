package org.example.uhy.domain.model.coupon.event;

import org.example.uhy.domain.mechanism.DomainEvent;
import org.example.uhy.domain.model.coupon.CouponId;

/**
 * 卡券已激活领域事件
 */
public class CouponActivated extends DomainEvent {
    private CouponId couponId;

    public CouponActivated(CouponId couponId) {
        this.couponId = couponId;
    }

    public String getCouponId() {
        return couponId.getId();
    }
}
