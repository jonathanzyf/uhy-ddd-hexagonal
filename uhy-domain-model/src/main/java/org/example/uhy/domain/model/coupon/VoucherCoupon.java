package org.example.uhy.domain.model.coupon;

import org.example.uhy.domain.model.base.TimeRange;
import org.example.uhy.supports.meta.Aggregate;

import java.math.BigDecimal;

/**
 * 代金券
 */
@Aggregate
public class VoucherCoupon extends Coupon {
    public static final String TYPE = "voucher"; // 代金券

    protected VoucherCoupon() {
        super();
    }

    public VoucherCoupon(CouponId couponId, CouponArchiveId archiveId, String name, TimeRange effectiveTime, CouponParameter parameter) {
        super(couponId, archiveId, name, effectiveTime, parameter);
    }

    public BigDecimal getQuota() {
        return parameter().getQuota();
    }

    public BigDecimal getDecrease() {
        return parameter().getDecrease();
    }
}
