package org.example.uhy.domain.model.coupon;

import org.example.uhy.domain.model.base.TimeRange;
import org.example.uhy.supports.meta.Aggregate;

import java.math.BigDecimal;

/**
 * 折扣卡券
 */
@Aggregate
public class DiscountCoupon extends Coupon {
    public static final String TYPE = "discount"; // 折扣券

    protected DiscountCoupon() {
        super();
    }

    public DiscountCoupon(CouponId couponId, CouponArchiveId archiveId, String name, TimeRange effectiveTime, CouponParameter parameter) {
        super(couponId, archiveId, name, effectiveTime, parameter);
    }

    public BigDecimal getDiscount() {
        return parameter().getDiscount();
    }
}
