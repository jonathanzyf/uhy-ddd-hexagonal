package org.example.uhy.domain.model.upgrade;

import org.example.uhy.domain.model.upgrade.execution.UpgradeExecutionContext;
import org.example.uhy.domain.model.upgrade.execution.UpgradeExecutionTree;
import org.example.uhy.domain.model.upgrade.execution.builder.UpgradeExecutionNodeFactory;
import org.example.uhy.domain.model.upgrade.execution.builder.UpgradeSchemaParser;
import org.example.uhy.domain.model.upgrade.schema.UpgradeSchema;
import org.example.uhy.supports.meta.DomainService;

@DomainService
public class UpgradeService {
    public boolean upgrade(UpgradeSchema schema, UpgradeExecutionContext executionContext) {
        UpgradeExecutionTree tree = new UpgradeSchemaParser().parse(schema, UpgradeExecutionNodeFactory.instance());
        return tree.match(executionContext);
    }
}