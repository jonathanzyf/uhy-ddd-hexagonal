package org.example.uhy.domain.model.upgrade.schema;

import org.example.uhy.domain.model.member.MemberLevelId;
import org.example.uhy.supports.meta.Aggregate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 晋级方案
 */
@Aggregate
public class UpgradeSchema {
    private String name;
    private MemberLevelId levelId;
    private Boolean keepLevelIfMismatching = false;
    private List<UpgradeRule> upgradeRules = new ArrayList<>();

    private UpgradeSchema() {
    }

    public UpgradeSchema(String name, MemberLevelId levelId, Boolean keepLevelIfMismatching) {
        this.setName(name);
        this.levelId(levelId);
        this.setKeepLevelIfMismatching(keepLevelIfMismatching);
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        if (name == null || name.length() == 0) {
            throw new IllegalArgumentException("请输入晋级方案名称");
        }
        this.name = name;
    }

    public MemberLevelId levelId() {
        return levelId;
    }

    private void levelId(MemberLevelId levelId) {
        if (levelId == null) {
            throw new IllegalArgumentException("请输入会员级别ID");
        }
        this.levelId = levelId;
    }

    private String getLevelId() {
        return levelId.getId();
    }

    private void setLevelId(String levelId) {
        this.levelId = MemberLevelId.of(levelId);
    }

    public Boolean getKeepLevelIfMismatching() {
        return keepLevelIfMismatching;
    }

    private void setKeepLevelIfMismatching(Boolean keepLevelIfMismatching) {
        this.keepLevelIfMismatching = keepLevelIfMismatching;
    }

    public void addUpgradeRule(UpgradeRule upgradeRule) {
        upgradeRules.add(upgradeRule);
    }

    public boolean hasUpgradeRule() {
        return upgradeRules.size() > 0;
    }

    public Iterator<UpgradeRule> ruleIterator() {
        return upgradeRules.iterator();
    }

    private List<UpgradeRule> getUpgradeRules() {
        return upgradeRules;
    }

    private void setUpgradeRules(List<UpgradeRule> upgradeRules) {
        this.upgradeRules = upgradeRules;
    }
}
