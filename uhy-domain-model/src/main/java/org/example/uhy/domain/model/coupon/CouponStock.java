package org.example.uhy.domain.model.coupon;

import org.example.uhy.exceptions.BusinessException;
import org.example.uhy.exceptions.IllegalParameterException;
import org.example.uhy.supports.meta.ValueObject;

/**
 * 卡券库存
 */
@ValueObject
public class CouponStock {
    private Integer plannedQuantity; // 预计发放数量，相当于库存总量
    private Integer currentQuantity; // 可用数量，相当于库存可用量

    private CouponStock() {
    }

    public CouponStock(Integer plannedQuantity, Integer quantity) {
        this.setPlannedQuantity(plannedQuantity);
        this.setQuantity(quantity);
    }

    public void reduce(Integer acquiredQuantity) {
        if (this.currentQuantity < acquiredQuantity) {
            throw new BusinessException("卡券可用数量不足");
        } else {
            this.currentQuantity -= acquiredQuantity;
        }
    }

    public Integer getPlannedQuantity() {
        return plannedQuantity;
    }

    private void setPlannedQuantity(Integer plannedQuantity) {
        if (plannedQuantity == null || plannedQuantity <= 0) {
            throw new IllegalParameterException("请输入卡券发放数量");
        }
        this.plannedQuantity = plannedQuantity;
    }

    public Integer getQuantity() {
        return currentQuantity;
    }

    private void setQuantity(Integer quantity) {
        if (quantity == null || quantity <= 0) {
            throw new IllegalParameterException("请输入卡券可用数量");
        }
        this.currentQuantity = quantity;
    }
}
