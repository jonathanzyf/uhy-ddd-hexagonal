package org.example.uhy.domain.registry;

import org.example.uhy.domain.mechanism.DomainEventPublisher;
import org.example.uhy.domain.model.coupon.CouponAdmissionService;
import org.example.uhy.domain.model.coupon.CouponArchiveRepository;
import org.example.uhy.domain.model.coupon.CouponRepository;

import java.util.function.BiFunction;

public class DomainRegistry {
    private static DomainRegistry _instance;
    CouponAdmissionService admissionService;
    CouponArchiveRepository couponArchiveRepository;
    CouponRepository couponRepository;
    DomainEventPublisher domainEventPublisher;
    BiFunction<String, Class, Object> beanFinder;

    public DomainRegistry(CouponArchiveRepository couponArchiveRepository, CouponRepository couponRepository, DomainEventPublisher domainEventPublisher, BiFunction<String, Class, Object> beanFinder) {
        admissionService = new CouponAdmissionService();
        this.couponArchiveRepository = couponArchiveRepository;
        this.couponRepository = couponRepository;
        this.domainEventPublisher = domainEventPublisher;
        this.beanFinder = beanFinder;
        _instance = this;
    }

    public static DomainRegistry instance() {
        return _instance;
    }

    public CouponAdmissionService admissionService() {
        return admissionService;
    }

    public CouponArchiveRepository couponArchiveRepository() {
        return couponArchiveRepository;
    }

    public CouponRepository couponRepository() {
        return couponRepository;
    }

    public DomainEventPublisher eventPublisher() {
        return domainEventPublisher;
    }

    public <T> T getBean(String beanName, Class<T> clazz) {
        return (T) beanFinder.apply(beanName, clazz);
    }
}
