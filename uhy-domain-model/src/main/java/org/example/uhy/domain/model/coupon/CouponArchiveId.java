package org.example.uhy.domain.model.coupon;

import org.example.uhy.exceptions.IllegalParameterException;
import org.example.uhy.supports.meta.ValueObject;

/**
 * 卡券定义ID
 */
@ValueObject
public class CouponArchiveId {
    private String id;

    private CouponArchiveId() {
    }

    private CouponArchiveId(String id) {
        this.setId(id);
    }

    public static CouponArchiveId of(String id) {
        return new CouponArchiveId(id);
    }

    public String getId() {
        return id;
    }

    private void setId(String id) {
        if (id == null || id.length() == 0) {
            throw new IllegalParameterException("请输入卡券定义ID");
        }
        this.id = id;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CouponArchiveId) {
            return id.equals(((CouponArchiveId) obj).id);
        }
        return false;
    }
}
