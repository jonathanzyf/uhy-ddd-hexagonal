package org.example.uhy.domain.model.trade;

import org.example.uhy.exceptions.IllegalParameterException;
import org.example.uhy.supports.meta.ValueObject;

/**
 * 交易ID
 */
@ValueObject
public class TradeId {
    private String tradeId;

    private TradeId() {
    }

    private TradeId(String tradeId) {
        this.setTradeId(tradeId);
    }

    public static TradeId of(String tradeId) {
        return new TradeId(tradeId);
    }

    public String getTradeId() {
        return tradeId;
    }

    private void setTradeId(String tradeId) {
        if (tradeId == null || tradeId.length() == 0) {
            throw new IllegalParameterException("请输入交易单据ID");
        }
        this.tradeId = tradeId;
    }
}
