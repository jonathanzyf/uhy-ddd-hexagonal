package org.example.uhy.domain.model.trade;

import org.example.uhy.domain.mechanism.DomainEvent;

/**
 * 交易完成事件
 */
@org.example.uhy.supports.meta.DomainEvent
public class MemberTraded extends DomainEvent {
    private TradeId tradeId;

    private MemberTraded() {
    }

    public MemberTraded(TradeId tradeId) {
        this.tradeId(tradeId);
    }

    public TradeId tradeId() {
        return tradeId;
    }

    // for constructor
    private void tradeId(TradeId tradeId) {
        if (tradeId == null) {
            throw new IllegalArgumentException("请输入交易单据ID");
        }
        this.tradeId = tradeId;
    }

    private String getTradeId() {
        return tradeId.getTradeId();
    }

    private void setTradeId(String tradeId) {
        this.tradeId = TradeId.of(tradeId);
    }
}
