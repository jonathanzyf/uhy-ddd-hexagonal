package org.example.uhy.domain.model.member;

import org.example.uhy.exceptions.IllegalParameterException;
import org.example.uhy.supports.meta.ValueObject;

import java.util.regex.Pattern;

/**
 * 手机号码
 */
@ValueObject
public class Phone {
    private static final Pattern pattern = Pattern.compile("1[3-9][0-9]{9}");
    private static final int PHONE_LENGTH = 11;

    private String phone;

    protected Phone() {
    }

    protected Phone(String phone) {
        this.setPhone(phone);
    }

    public static Phone of(String phone) {
        return new Phone(phone);
    }

    public String getPhone() {
        return phone;
    }

    private void setPhone(String phone) {
        if (phone == null || phone.length() == 0) {
            throw new IllegalParameterException("未传入手机号码");
        }
        if (phone.length() != PHONE_LENGTH) {
            throw new IllegalParameterException("手机号码格式不正确");
        }
        if (!pattern.matcher(phone).find()) {
            throw new IllegalParameterException("手机号码格式不正确");
        }
        this.phone = phone;
    }
}
