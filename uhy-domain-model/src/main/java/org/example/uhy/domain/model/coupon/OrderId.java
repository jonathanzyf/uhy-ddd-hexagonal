package org.example.uhy.domain.model.coupon;

import org.example.uhy.supports.meta.ValueObject;

/**
 * 交易单号ID
 */
@ValueObject
public class OrderId {
    private String orderId;

    private OrderId() {
    }

    private OrderId(String orderId) {
        this.setOrderId(orderId);
    }

    public static OrderId of(String orderId) {
        return new OrderId(orderId);
    }

    public String getOrderId() {
        return orderId;
    }

    private void setOrderId(String orderId) {
        if (orderId == null || orderId.length() == 0) {
            throw new IllegalArgumentException("请输入交易单号ID");
        }
        this.orderId = orderId;
    }

    @Override
    public int hashCode() {
        return orderId.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof OrderId) {
            return orderId.equals(((OrderId) obj).orderId);
        }
        return false;
    }
}
