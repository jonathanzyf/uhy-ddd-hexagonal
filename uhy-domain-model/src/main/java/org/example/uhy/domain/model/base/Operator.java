package org.example.uhy.domain.model.base;

import org.example.uhy.exceptions.IllegalParameterException;
import org.example.uhy.supports.meta.ValueObject;

/**
 * 操作员
 */
@ValueObject
public class Operator {
    private String operatorId;
    private String name;

    private Operator() {
    }

    private Operator(String operatorId) {
        this.setOperatorId(operatorId);
    }

    private Operator(String operatorId, String name) {
        this.setOperatorId(operatorId);
        this.setName(name);
    }

    public static Operator of(String operatorId) {
        return new Operator(operatorId);
    }

    public static Operator of(String operatorId, String name) {
        return new Operator(operatorId, name);
    }

    public String getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(String operatorId) {
        if (operatorId == null || operatorId.length() == 0) {
            throw new IllegalParameterException("请输入卡券发放员ID");
        }
        this.operatorId = operatorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null || name.length() == 0) {
            throw new IllegalParameterException("请输入卡券发放员姓名");
        }
        this.name = name;
    }
}
