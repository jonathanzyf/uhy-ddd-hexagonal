package org.example.uhy.domain.model.coupon;

import org.example.uhy.supports.meta.Repository;

import java.util.Optional;

@Repository
public interface CouponArchiveRepository {
    void add(CouponArchive definition);

    Optional<CouponArchive> couponArchiveOfId(CouponArchiveId couponArchiveId);

    void save(CouponArchive couponArchive);

    /**
     * 生成卡券定义ID
     *
     * @return
     */
    CouponArchiveId nextIdentity();
}