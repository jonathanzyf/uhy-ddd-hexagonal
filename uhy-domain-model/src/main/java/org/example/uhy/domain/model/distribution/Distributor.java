package org.example.uhy.domain.model.distribution;

import org.example.uhy.supports.meta.Aggregate;

/**
 * 分销员
 */
@Aggregate
public class Distributor {
    private DistributorId distributorId;

    public Distributor(DistributorId distributorId) {
        this.distributorId(distributorId);
    }

    public DistributorId distributorId() {
        return distributorId;
    }

    public void distributorId(DistributorId distributorId) {
        if (distributorId == null) {
            throw new IllegalArgumentException("请输入分销员ID");
        }
        this.distributorId = distributorId;
    }

    private String getId() {
        return distributorId.getId();
    }

    private void setId(String distributorId) {
        this.distributorId = DistributorId.of(distributorId);
    }
}
