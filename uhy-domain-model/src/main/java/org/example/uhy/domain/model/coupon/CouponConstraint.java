package org.example.uhy.domain.model.coupon;

import org.example.uhy.exceptions.IllegalParameterException;
import org.example.uhy.supports.meta.ValueObject;

/**
 * 卡券发放约束条件定义
 */
@ValueObject
public class CouponConstraint {
    private String scope; // 适用范围
    private Integer countPerOwner; // 每人限领数量
//    private Boolean allowTransfer;
//    private Boolean allowSale;
//    private Boolean allowReceive;

    private CouponConstraint() {
    }

    public CouponConstraint(String scope, Integer countPerOwner) {
        this.setScope(scope);
        this.setCountPerOwner(countPerOwner);
    }

    /**
     * 卡券资格检查
     *
     * @deprecated 使用{@link CouponAdmissionService}进行卡券资格检查
     * <p>
     * 根据发放范围、领用数量进行校验
     * PS：这里其实是卡券发放约束条件定义，真正执行约束检查可以构造一颗规则检查树，所需参数在构造规则检查树节点时传入
     * 如果规则检查没有and和or的关系，规则检查实例通过Filter模式依次执行也可以。
     */
    @Deprecated
    public void check() {

    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        if (scope == null || scope.length() == 0) {
            throw new IllegalParameterException("请输入卡券适用范围");
        }
        this.scope = scope;
    }

    public Integer getCountPerOwner() {
        return countPerOwner;
    }

    public void setCountPerOwner(Integer countPerOwner) {
        if (countPerOwner == null || countPerOwner <= 0) {
            throw new IllegalParameterException("请输入卡券每人限领张数");
        }
        this.countPerOwner = countPerOwner;
    }
}
