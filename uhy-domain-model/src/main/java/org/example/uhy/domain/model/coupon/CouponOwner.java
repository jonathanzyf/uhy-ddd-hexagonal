package org.example.uhy.domain.model.coupon;

import org.example.uhy.domain.model.fans.FansId;
import org.example.uhy.domain.model.member.MemberId;
import org.example.uhy.exceptions.IllegalParameterException;
import org.example.uhy.supports.meta.ValueObject;

/**
 * 卡券所属人
 */
@ValueObject
public class CouponOwner {
    private String ownerId;
    private String name;
    private boolean isMember;

    private CouponOwner() {
    }

    private CouponOwner(String ownerId, boolean isMember) {
        this.setOwnerId(ownerId);
        this.setMember(isMember);
    }

    private CouponOwner(String ownerId, String name, boolean isMember) {
        this(ownerId, isMember);
        this.setName(name);
    }

    public static CouponOwner of(MemberId memberId) {
        return new CouponOwner(memberId.getId(), true);
    }

    public static CouponOwner of(FansId fansId) {
        return new CouponOwner(fansId.getFansId(), false);
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        if (ownerId == null || ownerId.length() == 0) {
            throw new IllegalParameterException("请输入卡券所属人ID");
        }
        this.ownerId = ownerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null || name.length() == 0) {
            throw new IllegalParameterException("请输入卡券所属人姓名");
        }
        this.name = name;
    }

    public boolean isMember() {
        return isMember;
    }

    public void setMember(boolean member) {
        isMember = member;
    }

    @Override
    public int hashCode() {
        return ownerId.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CouponOwner) {
            return this.ownerId.equals(((CouponOwner) obj).getOwnerId());
        }
        return false;
    }
}
