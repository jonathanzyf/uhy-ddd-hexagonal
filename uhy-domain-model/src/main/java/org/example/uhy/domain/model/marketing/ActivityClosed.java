package org.example.uhy.domain.model.marketing;

import org.example.uhy.domain.mechanism.DomainEvent;

/**
 * 营销活动结束事件
 */
@org.example.uhy.supports.meta.DomainEvent
public class ActivityClosed extends DomainEvent {
    private ActivityId activityId;

    private ActivityClosed() {
    }

    public ActivityClosed(ActivityId activityId) {
        this.activityId(activityId);
    }

    public ActivityId activityId() {
        return activityId;
    }

    private void activityId(ActivityId activityId) {
        if (activityId == null) {
            throw new IllegalArgumentException("请输入活动ID");
        }
        this.activityId = activityId;
    }

    private String getActivityId() {
        return activityId.getActivityId();
    }

    private void setActivityId(String activityId) {
        this.activityId = ActivityId.of(activityId);
    }
}
