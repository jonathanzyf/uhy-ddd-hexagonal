package org.example.uhy.domain.model.marketing;

import org.example.uhy.supports.meta.ValueObject;

/**
 * 营销活动ID
 */
@ValueObject
public class ActivityId {
    private String activityId;

    public ActivityId(String activityId) {
        this.setActivityId(activityId);
    }

    public static ActivityId of(String activityId) {
        return new ActivityId(activityId);
    }

    public String getActivityId() {
        return activityId;
    }

    private void setActivityId(String activityId) {
        if (activityId == null || activityId.length() == 0) {
            throw new IllegalArgumentException("请输入活动ID");
        }
        this.activityId = activityId;
    }
}
