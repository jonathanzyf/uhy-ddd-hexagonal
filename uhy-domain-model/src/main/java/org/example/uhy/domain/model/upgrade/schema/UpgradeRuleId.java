package org.example.uhy.domain.model.upgrade.schema;

import org.example.uhy.supports.meta.ValueObject;

/**
 * 晋级规则ID
 */
@ValueObject
public class UpgradeRuleId {
    private String id;

    private UpgradeRuleId() {
    }

    protected UpgradeRuleId(String id) {
        this.setId(id);
    }

    public static UpgradeRuleId of(String id) {
        return new UpgradeRuleId(id);
    }

    public String getId() {
        return id;
    }

    private void setId(String id) {
        if (id == null || id.length() == 0) {
            throw new IllegalArgumentException("请输入晋级规则ID");
        }
        this.id = id;
    }

    @Override
    public int hashCode() {
        return this.getId().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof UpgradeRuleId) {
            return this.getId().equals(((UpgradeRuleId) obj).getId());
        }
        return false;
    }
}
