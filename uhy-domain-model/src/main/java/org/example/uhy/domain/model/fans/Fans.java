package org.example.uhy.domain.model.fans;

import org.example.uhy.supports.meta.Entity;

/**
 * 粉丝
 */
@Entity
public class Fans {
    private FansId fansId;
    private String name;

    private Fans() {
    }

    public Fans(FansId fansId, String name) {
        this.fansId(fansId);
        this.setName(name);
    }

    public FansId fansId() {
        return fansId;
    }

    public void fansId(FansId fansId) {
        if (fansId == null) {
            throw new IllegalArgumentException("请输入粉丝ID");
        }
        this.fansId = fansId;
    }

    private String getOpenId() {
        return fansId.getFansId();
    }

    private void setOpenId(String openId) {
        this.fansId = FansId.of(openId);
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        if (name == null || name.length() == 0) {
            throw new IllegalArgumentException("请输入粉丝昵称");
        }
        this.name = name;
    }
}
