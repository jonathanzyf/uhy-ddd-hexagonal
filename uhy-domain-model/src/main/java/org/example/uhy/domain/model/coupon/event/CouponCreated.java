package org.example.uhy.domain.model.coupon.event;

import org.example.uhy.domain.mechanism.DomainEvent;
import org.example.uhy.domain.model.coupon.CouponId;

/**
 * 卡券已创建领域事件
 */
public class CouponCreated extends DomainEvent {
    private CouponId couponId;

    public CouponCreated(CouponId couponId) {
        this.couponId = couponId;
    }

    public String getCouponId() {
        return couponId.getId();
    }
}
