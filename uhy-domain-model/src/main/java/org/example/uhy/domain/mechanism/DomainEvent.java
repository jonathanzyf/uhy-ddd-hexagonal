package org.example.uhy.domain.mechanism;

import java.time.LocalDateTime;
import java.util.UUID;

public class DomainEvent {
    private String key;
    private LocalDateTime createTime;

    public DomainEvent() {
        this.createTime = LocalDateTime.now();
        this.key = UUID.randomUUID().toString();
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public final String getKey() {
        return this.key;
    }

    public final String getClazz() {
        return this.getClass().getSimpleName();
    }
}
