package org.example.uhy.domain.model.member;

import org.example.uhy.domain.mechanism.DomainEvent;

/**
 * 会员信息完善事件
 */
@org.example.uhy.supports.meta.DomainEvent
public class MemberInfoComplemented extends DomainEvent {
    private MemberId memberId;

    private MemberInfoComplemented() {
    }

    public MemberInfoComplemented(MemberId memberId) {
        this.memberId(memberId);
    }

    public MemberId memberId() {
        return memberId;
    }

    private void memberId(MemberId memberId) {
        if (memberId == null) {
            throw new IllegalArgumentException("请输入会员ID");
        }
        this.memberId = memberId;
    }

    private MemberId getMemberId() {
        return memberId;
    }

    private void setMemberId(String memberId) {
        this.memberId = MemberId.of(memberId);
    }
}