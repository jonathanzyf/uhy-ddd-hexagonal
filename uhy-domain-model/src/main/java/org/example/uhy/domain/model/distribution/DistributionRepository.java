package org.example.uhy.domain.model.distribution;

import org.example.uhy.domain.model.member.MemberId;
import org.example.uhy.supports.meta.Repository;

import java.util.List;

/**
 * 分销仓库
 */
@Repository
public interface DistributionRepository {
    /**
     * 获取分销员所发展的会员数量
     *
     * @param distributorId 分销员ID
     * @return
     */
    Integer membersCountOfDistributor(DistributorId distributorId);

    /**
     * 获取分销员所发展的会员
     *
     * @param distributorId 分销员ID
     * @return 会员ID集合
     */
    List<MemberId> membersOfDistributor(DistributorId distributorId);
}
