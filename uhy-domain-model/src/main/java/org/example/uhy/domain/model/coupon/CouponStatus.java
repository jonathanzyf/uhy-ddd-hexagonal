package org.example.uhy.domain.model.coupon;

public enum CouponStatus {
    Created, Activated, Consumed, Expired, Cancelled
}