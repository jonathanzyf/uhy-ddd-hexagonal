package org.example.uhy.domain.model.fans;

import org.example.uhy.exceptions.IllegalParameterException;
import org.example.uhy.supports.meta.ValueObject;

/**
 * 粉丝ID
 */
@ValueObject
public class FansId {
    private String fansId;

    private FansId() {
    }

    private FansId(String fansId) {
        this.setFansId(fansId);
    }

    public static FansId of(String fansId) {
        return new FansId(fansId);
    }

    public String getFansId() {
        return fansId;
    }

    private void setFansId(String fansId) {
        if (fansId == null || fansId.length() == 0) {
            throw new IllegalParameterException("请输入粉丝ID");
        }
        this.fansId = fansId;
    }
}
