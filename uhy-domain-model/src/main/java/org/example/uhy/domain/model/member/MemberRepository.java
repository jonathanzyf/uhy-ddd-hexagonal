package org.example.uhy.domain.model.member;

import org.example.uhy.supports.meta.Repository;

import java.util.List;
import java.util.Optional;

/**
 * 会员仓库接口定义
 */
@Repository
public interface MemberRepository {
    /**
     * 根据会员ID查找会员
     *
     * @param memberId 会员ID
     * @return 会员
     */
    Optional<Member> memberOfId(MemberId memberId);

    /**
     * 根据会员标签查找会员
     *
     * @param label 会员标签
     * @return 会员集合
     */
    List<Member> membersOfLabel(MemberLabel label);

    /**
     * 根据会员级别查找会员
     *
     * @param level 会员级别
     * @return 会员集合
     */
    List<Member> membersOfLevel(MemberLevelId level);

    /**
     * 根据会员标签和级别查找会员
     *
     * @param label
     * @param level
     * @return
     */
    List<Member> members(MemberLabel label, MemberLevelId level);

    /**
     * 新增会员
     *
     * @param member 会员对象
     * @return
     */
    void add(Member member);

    /**
     * 保存会员
     *
     * @param member 会员对象
     * @return
     */
    void save(Member member);

    /**
     * 生成会员ID
     *
     * @return
     */
    MemberId nextIdentity();
}