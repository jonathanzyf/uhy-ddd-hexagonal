package org.example.uhy.domain.model.upgrade.execution.builder;

import org.example.uhy.domain.model.upgrade.execution.node.UpgradeExecutionNode;
import org.example.uhy.domain.model.upgrade.schema.UpgradeRule;

@FunctionalInterface
public interface UpgradeExecutionNodeBuilder {
    UpgradeExecutionNode apply(UpgradeRule rule);
}