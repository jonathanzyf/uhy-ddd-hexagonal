package org.example.uhy.domain.model.upgrade.execution.node;

import org.example.uhy.domain.model.member.MemberId;
import org.example.uhy.domain.model.upgrade.execution.UpgradeExecutionContext;

import java.util.List;

public abstract class ConditionUpgradeExecutionNode<T> implements UpgradeExecutionNode {
    private static final String MEMBERS_KEY = "members";
    private T expectedValue;

    public ConditionUpgradeExecutionNode(T expectedValue) {
        this.expectedValue = expectedValue;
    }

    public T getExpectedValue() {
        return expectedValue;
    }

    public abstract String getType();

    protected List<MemberId> membersOfContributor(UpgradeExecutionContext context) {
        if (context.upgradeObject().isMember()) {
            return null;
        }
        if (context.hasCachedValue(MEMBERS_KEY)) {
            return context.getCachedValue(MEMBERS_KEY);
        } else {
            List<MemberId> memberIds = context.distributionRepository().membersOfDistributor(context.upgradeObject().distributorId());
            context.setCachedValue(MEMBERS_KEY, memberIds);
            return memberIds;
        }
    }

    @Override
    public String toString() {
        return getType() + "\t" + expectedValue;
    }
}