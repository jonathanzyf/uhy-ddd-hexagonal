package org.example.uhy.domain.model.trade;

import org.example.uhy.domain.model.member.MemberId;
import org.example.uhy.supports.meta.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * 交易仓库接口定义
 */
@Repository
public interface TradeRepository {
    /**
     * 根据交易ID获取交易记录
     *
     * @param tradeId 交易ID
     * @return 交易
     */
    Trade tradeOfId(TradeId tradeId);

    /**
     * 会员的累计消费金额
     *
     * @param memberId 会员ID
     * @return 会员累计消费金额
     */
    BigDecimal accumulationMoneyOfMember(MemberId memberId);

    /**
     * 一批会员的累计消费金额
     *
     * @param memberIds 会员ID集合
     * @return 所有会员累计消费金额
     */
    BigDecimal accumulationMoneyOfMembers(List<MemberId> memberIds);

    /**
     * 会员的累计消费次数
     *
     * @param memberId 会员ID
     * @return 会员累计消费次数
     */
    Integer accumulationCountOfMember(MemberId memberId);

    /**
     * 一批会员的消费次数
     *
     * @param memberIds 会员ID集合
     * @return 所有会员累计消费次数
     */
    Integer accumulationCountOfMembers(List<MemberId> memberIds);
}
