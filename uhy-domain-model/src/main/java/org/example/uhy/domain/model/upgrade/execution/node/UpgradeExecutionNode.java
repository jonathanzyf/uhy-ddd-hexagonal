package org.example.uhy.domain.model.upgrade.execution.node;

import org.example.uhy.domain.model.upgrade.execution.UpgradeExecutionContext;

public interface UpgradeExecutionNode {
    boolean match(UpgradeExecutionContext context);
}