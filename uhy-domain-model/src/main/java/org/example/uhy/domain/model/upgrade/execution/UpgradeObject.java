package org.example.uhy.domain.model.upgrade.execution;

import org.example.uhy.domain.model.distribution.DistributorId;
import org.example.uhy.domain.model.member.MemberId;
import org.example.uhy.supports.meta.ValueObject;

/**
 * 晋级对象
 * <p>
 * 晋级对象可以是会员和促销员
 */
@ValueObject
public class UpgradeObject {
    private MemberId memberId;
    private DistributorId distributorId;
    private boolean isMember;

    private UpgradeObject() {
    }

    private UpgradeObject(MemberId memberId) {
        this.memberId(memberId);
    }

    private UpgradeObject(DistributorId distributorId) {
        this.distributorId(distributorId);
    }

    public static UpgradeObject of(MemberId memberId) {
        return new UpgradeObject(memberId);
    }

    public static UpgradeObject of(DistributorId distributorId) {
        return new UpgradeObject(distributorId);
    }

    public MemberId memberId() {
        return memberId;
    }

    private void memberId(MemberId memberId) {
        if (memberId == null) {
            throw new IllegalArgumentException("请输入晋级对象ID");
        }
        this.memberId = memberId;
        this.isMember = true;
    }

    public DistributorId distributorId() {
        return distributorId;
    }

    private void distributorId(DistributorId distributorId) {
        if (distributorId == null) {
            throw new IllegalArgumentException("请输入晋级对象ID");
        }
        this.distributorId = distributorId;
        this.isMember = false;
    }

    public boolean isMember() {
        return isMember;
    }
}
