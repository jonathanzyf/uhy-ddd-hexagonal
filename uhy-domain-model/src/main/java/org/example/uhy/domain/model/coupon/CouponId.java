package org.example.uhy.domain.model.coupon;

import org.example.uhy.exceptions.IllegalParameterException;
import org.example.uhy.supports.meta.ValueObject;

/**
 * 卡券ID
 */
@ValueObject
public class CouponId {
    private String id;

    private CouponId() {
    }

    private CouponId(String couponId) {
        this.setId(couponId);
    }

    public static CouponId of(String couponId) {
        return new CouponId(couponId);
    }

    public String getId() {
        return id;
    }

    private void setId(String couponId) {
        if (couponId == null || couponId.length() == 0) {
            throw new IllegalParameterException("请输入卡券ID");
        }
        this.id = couponId;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CouponId) {
            return id.equals(((CouponId) obj).id);
        }
        return false;
    }
}
