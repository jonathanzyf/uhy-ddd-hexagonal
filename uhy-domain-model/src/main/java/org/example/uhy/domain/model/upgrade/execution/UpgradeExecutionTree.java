package org.example.uhy.domain.model.upgrade.execution;


import org.example.uhy.domain.model.upgrade.execution.node.UpgradeExecutionNode;

/**
 * 晋级执行树
 */
public class UpgradeExecutionTree {
    private UpgradeExecutionNode root;

    public UpgradeExecutionTree(UpgradeExecutionNode root) {
        this.root = root;
    }

    public boolean match(UpgradeExecutionContext context) {
        return root.match(context);
    }

    public UpgradeExecutionNode getRoot() {
        return root;
    }
}