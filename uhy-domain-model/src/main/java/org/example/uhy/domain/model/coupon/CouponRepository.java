package org.example.uhy.domain.model.coupon;

import org.example.uhy.supports.meta.Repository;

import java.util.List;
import java.util.Optional;

/**
 * 卡券仓库
 */
@Repository
public interface CouponRepository {
    Optional<Coupon> couponOfId(CouponId couponId);

    void add(Coupon coupon);

    void save(Coupon coupon);

    List<Coupon> scanExpiredCoupons();

    Integer countOfOwner(CouponArchiveId archiveId, CouponOwner owner);

    CouponId nextIdentity();
}