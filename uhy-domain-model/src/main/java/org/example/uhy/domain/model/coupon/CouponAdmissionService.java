package org.example.uhy.domain.model.coupon;

import org.example.uhy.domain.model.member.MemberLevelId;
import org.example.uhy.exceptions.BusinessException;
import org.example.uhy.supports.meta.DomainService;

import java.util.function.Supplier;

/**
 * 负责卡券资格检查
 * 检查{@link CouponOwner}对象是否有资格获取、售卖、转让卡券
 */
@DomainService
public class CouponAdmissionService {
    /**
     * 获取卡券授权
     *
     * @param constraint
     * @param receivedQuantitySupplier 获取已经拥有的卡券数量
     * @param owner
     */
    public void acquireAdmission(CouponConstraint constraint, Supplier<Integer> receivedQuantitySupplier, CouponOwner owner) {
        // iReceiveLimit 可自领数量
        // cLimitMemberLevelIDs 限制会员身份及等级
        Integer countPerOwner = constraint.getCountPerOwner();
        if (countPerOwner != null) {
            if (receivedQuantitySupplier.get() >= countPerOwner) {
                throw new BusinessException("超出卡券领用限制，每人最多领用" + countPerOwner + "张");
            }
        }
    }

    /**
     * 获取卡券授权
     *
     * @param constraint
     * @param receivedQuantitySupplier 获取已经拥有的卡券数量
     * @param owner
     * @param levelId
     */
    public void acquireAdmission(CouponConstraint constraint, Supplier<Integer> receivedQuantitySupplier, CouponOwner owner, MemberLevelId levelId) {
        // iReceiveLimit 可自领数量
        // cLimitMemberLevelIDs 限制会员身份及等级
    }

    /**
     * 转移授权
     *
     * @param constraint
     * @param owner
     */
    public void transferAdmission(CouponConstraint constraint, CouponOwner owner) {

    }

    /**
     * 售卖授权
     *
     * @param constraint
     * @param owner
     */
    public void saleAdmission(CouponConstraint constraint, CouponOwner owner) {

    }
}
