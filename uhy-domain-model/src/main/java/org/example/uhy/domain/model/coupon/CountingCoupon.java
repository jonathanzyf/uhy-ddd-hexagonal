package org.example.uhy.domain.model.coupon;

import org.example.uhy.domain.model.base.TimeRange;
import org.example.uhy.supports.meta.Aggregate;

/**
 * 计次卡券
 */
@Aggregate
public class CountingCoupon extends Coupon {
    public static final String TYPE = "counting"; // 计次券

    protected CountingCoupon() {
        super();
    }

    public CountingCoupon(CouponId couponId, CouponArchiveId archiveId, String name, TimeRange effectiveTime, CouponParameter parameter) {
        super(couponId, archiveId, name, effectiveTime, parameter);
    }

    public Integer getCounting() {
        return parameter().getCounting();
    }

    public void decrease(int count) {
        parameter().decreaseCounting(count);
    }
}
