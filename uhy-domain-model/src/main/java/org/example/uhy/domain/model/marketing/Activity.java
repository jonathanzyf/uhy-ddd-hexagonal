package org.example.uhy.domain.model.marketing;

import org.example.uhy.supports.meta.Aggregate;

/**
 * 营销活动
 */
@Aggregate
public class Activity {
    private ActivityId activityId;

    private Activity() {
    }

    private Activity(ActivityId activityId) {
        this.activityId(activityId);
    }

    public ActivityId activityId() {
        return activityId;
    }

    // for constructor
    private void activityId(ActivityId activityId) {
        if (activityId == null) {
            throw new IllegalArgumentException("请输入营销活动ID");
        }
        this.activityId = activityId;
    }

    // for orm
    private String getId() {
        return activityId.getActivityId();
    }

    // for orm
    private void setId(String activityId) {
        this.activityId = ActivityId.of(activityId);
    }
}