package org.example.uhy.domain.model.coupon.event;

import org.example.uhy.domain.mechanism.DomainEvent;
import org.example.uhy.domain.model.coupon.CouponId;

/**
 * 卡券已消费领域事件
 */
public class CouponConsumed extends DomainEvent {
    private CouponId couponId;

    public CouponConsumed(CouponId couponId) {
        this.couponId = couponId;
    }

    public String getCouponId() {
        return couponId.getId();
    }
}
