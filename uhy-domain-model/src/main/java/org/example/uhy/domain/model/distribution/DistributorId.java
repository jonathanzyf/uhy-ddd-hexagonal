package org.example.uhy.domain.model.distribution;

import org.example.uhy.supports.meta.ValueObject;

/**
 * 分销员ID
 */
@ValueObject
public class DistributorId {
    private String id;

    private DistributorId() {
    }

    private DistributorId(String id) {
        this.setId(id);
    }

    public static DistributorId of(String id) {
        return new DistributorId(id);
    }

    public String getId() {
        return id;
    }

    private void setId(String id) {
        if (id == null || id.length() == 0) {
            throw new IllegalArgumentException("请输入分销员ID");
        }
        this.id = id;
    }
}
