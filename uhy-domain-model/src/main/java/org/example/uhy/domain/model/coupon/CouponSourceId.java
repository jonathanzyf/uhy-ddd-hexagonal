package org.example.uhy.domain.model.coupon;

import org.example.uhy.domain.model.marketing.ActivityId;
import org.example.uhy.domain.model.trade.TradeId;
import org.example.uhy.supports.meta.ValueObject;

/**
 * 卡券来源ID
 */
@ValueObject
public class CouponSourceId {
    public static final CouponSourceId EMPTY = new CouponSourceId();
    private String sourceId;
    private String type;

    private CouponSourceId() {
    }

    private CouponSourceId(String type, String sourceId) {
        this.setType(type);
        this.setSourceId(sourceId);
    }

    public static CouponSourceId of(ActivityId activityId) {
        return new CouponSourceId("activity", activityId.getActivityId());
    }

    public static CouponSourceId of(TradeId tradeId) {
        return new CouponSourceId("trade", tradeId.getTradeId());
    }

    public static CouponSourceId of(OrderId orderId) {
        return new CouponSourceId("buy", orderId.getOrderId());
    }

    public static CouponSourceId of(String type, String sourceId) {
        return new CouponSourceId(type, sourceId);
    }

    public String getSourceId() {
        return sourceId;
    }

    private void setSourceId(String sourceId) {
        if (sourceId == null || sourceId.length() == 0) {
            throw new IllegalArgumentException("请输入来源ID");
        }
        this.sourceId = sourceId;
    }

    public String getType() {
        return type;
    }

    private void setType(String type) {
        if (type == null || type.length() == 0) {
            throw new IllegalArgumentException("请输入来源类型");
        }
        this.type = type;
    }
}
