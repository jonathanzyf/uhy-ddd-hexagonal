package org.example.uhy.domain.model.upgrade.schema;

public enum LogicalOperator {
    AND, OR
}
