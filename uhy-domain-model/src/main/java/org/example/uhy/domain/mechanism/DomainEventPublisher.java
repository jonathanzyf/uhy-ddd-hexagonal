package org.example.uhy.domain.mechanism;

public interface DomainEventPublisher {
    void publish(DomainEvent event);
}