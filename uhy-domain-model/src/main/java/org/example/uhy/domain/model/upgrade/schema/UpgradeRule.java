package org.example.uhy.domain.model.upgrade.schema;

import org.example.uhy.supports.meta.Entity;

import java.util.Optional;

/**
 * 晋级规则
 */
@Entity
public class UpgradeRule {
    private UpgradeRuleId ruleId;
    private UpgradeRuleId parentRuleId;
    private UpgradeRuleType ruleType;
    private LogicalOperator operator;
    private String value;

    private UpgradeRule() {
    }

    public UpgradeRule(UpgradeRuleId ruleId, UpgradeRuleId parentRuleId, UpgradeRuleType ruleType, LogicalOperator operator, String value) {
        this.ruleId(ruleId);
        this.parentRuleId(parentRuleId);
        this.ruleType(ruleType);
        this.operator(operator);
        this.setValue(value);
        checkOperatorAndRuleType();
    }

    private void checkOperatorAndRuleType() {
        if (ruleType == UpgradeRuleType.LOGICAL && operator == null) {
            throw new IllegalArgumentException("请输入逻辑运算符或者规则类型");
        }
    }

    public UpgradeRuleId ruleId() {
        return ruleId;
    }

    private void ruleId(UpgradeRuleId ruleId) {
        if (ruleId == null) {
            throw new IllegalArgumentException("请输入晋级规则ID");
        }
        this.ruleId = ruleId;
    }

    private String getId() {
        return ruleId.getId();
    }

    private void setId(String id) {
        this.ruleId = UpgradeRuleId.of(id);
    }

    public Optional<UpgradeRuleId> parentRuleId() {
        return Optional.ofNullable(parentRuleId);
    }

    public void parentRuleId(UpgradeRuleId parentRuleId) {
        this.parentRuleId = parentRuleId;
    }

    private String getParentRuleId() {
        return parentRuleId.getId();
    }

    private void setParentRuleId(String parentRuleId) {
        if (parentRuleId != null && parentRuleId.length() > 0) {
            this.parentRuleId = UpgradeRuleId.of(parentRuleId);
        }
    }

    public UpgradeRuleType ruleType() {
        return ruleType;
    }

    private void ruleType(UpgradeRuleType ruleType) {
        if (ruleType == null) {
            ruleType = UpgradeRuleType.LOGICAL;
        }
        this.ruleType = ruleType;
    }

    private String getRuleType() {
        return ruleType.getRuleType();
    }

    private void setRuleType(String ruleType) {
        this.ruleType = UpgradeRuleType.of(ruleType);
    }

    public Optional<LogicalOperator> operator() {
        return Optional.ofNullable(operator);
    }

    public void operator(LogicalOperator operator) {
        this.operator = operator;
    }

    private String getOperator() {
        return operator.name();
    }

    private void setOperator(String operator) {
        this.operator = LogicalOperator.valueOf(operator);
    }

    public Optional<String> value() {
        return Optional.ofNullable(value);
    }

    private String getValue() {
        return value;
    }

    private void setValue(String value) {
        this.value = value;
    }
}