package org.example.uhy.domain.model.member;

import org.example.uhy.exceptions.IllegalParameterException;
import org.example.uhy.supports.meta.ValueObject;

/**
 * 会员标签
 */
@ValueObject
public class MemberLabel {
    private String label;

    private MemberLabel() {
    }

    public MemberLabel(String label) {
        this.setLabel(label);
    }

    public static MemberLabel of(String label) {
        return new MemberLabel(label);
    }

    public String getLabel() {
        return label;
    }

    private void setLabel(String label) {
        if (label == null || label.length() == 0) {
            throw new IllegalParameterException("请输入会员标签");
        }
        this.label = label;
    }

    @Override
    public int hashCode() {
        return label.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MemberLabel) {
            return label.equals(((MemberLabel) obj).label);
        }
        return false;
    }
}
