package org.example.uhy.domain.model.member;

import java.util.Optional;

public interface MemberLevelRepository {
    MemberLevel defaultMemberLevel();

    Optional<MemberLevel> memberLevelOfId(MemberLevelId levelId);
}
