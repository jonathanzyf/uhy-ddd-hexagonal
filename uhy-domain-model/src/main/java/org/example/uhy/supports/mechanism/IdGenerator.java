package org.example.uhy.supports.mechanism;

public interface IdGenerator {
    String nextIdentity();
}
