package org.example.uhy.application.converter;

import org.example.uhy.application.dto.MemberInfo;
import org.example.uhy.domain.model.member.Member;
import org.springframework.stereotype.Component;

@Component
public class MemberConverter {
    public MemberInfo convert(Member member) {
        if (member == null) {
            return null;
        }
        MemberInfo memberInfo = new MemberInfo();
        memberInfo.setId(member.memberId().getId());
        memberInfo.setName(member.getName());
        memberInfo.setLevelId(member.levelId().getId());
        if (member.hasLabel()) {
            memberInfo.setLabel(member.label().get().getLabel());
        }
        memberInfo.setPhone(member.phone().getPhone());
        return memberInfo;
    }
}
