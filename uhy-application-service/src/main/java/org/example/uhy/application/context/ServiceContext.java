package org.example.uhy.application.context;

import org.example.uhy.application.exceptions.AuthenticationException;

public class ServiceContext {
    private String token;
    private String userId;

    private ServiceContext() {
    }

    public ServiceContext(String accessToken) {
        this.setToken(accessToken);
    }

    public static ServiceContext robot() {
        ServiceContext context = new ServiceContext();
        context.setToken("temporary-token");
        context.userId = "robot";
        return context;
    }

    public String currentTenantId() {
        return "a-tenant-id";
    }

    public String currentOrgId() {
        return "a-org-id";
    }

    public String currentFansId() {
        return "a-fans-id";
    }

    public String currentMemberId() {
        return "a-member-id";
    }

    public String currentOwner() {
        return "a-member-id"; // or a-fans-id
    }

    public String currentOperator() {
        return userId;
    }

    public String getToken() {
        return token;
    }

    private void setToken(String token) {
        if (token == null || token.length() == 0) {
            throw new AuthenticationException("未经授权的请求");
        }
        this.token = token;
        userId = "an-operator-id";
    }
}
