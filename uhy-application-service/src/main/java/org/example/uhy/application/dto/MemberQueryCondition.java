package org.example.uhy.application.dto;

import org.example.uhy.supports.meta.DTO;

@DTO
public class MemberQueryCondition {
    private String label;
    private String level;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
