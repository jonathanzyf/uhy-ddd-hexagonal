package org.example.uhy.application.dto;

import org.example.uhy.supports.meta.DTO;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@DTO
public class CouponInfo {
    private String id;
    private String archiveId;
    private String name;
    // 有效期
    private LocalDateTime effectiveFromTime;
    private LocalDateTime effectiveToTime;
    // 状态
    private Integer status;
    // 所有人
    private String operatorId;
    private String ownerId;
    private boolean isMember;
    // 操作时间
    private LocalDateTime createTime;
    private LocalDateTime sendTime;
    private LocalDateTime receiveTime;
    private LocalDateTime consumeTime;
    private LocalDateTime cancelTime;
    private LocalDateTime backTime;
    // 来源信息
    private String sourceId;
    private String sourceType;
    // 卡券参数
    private String type; // 卡券类型
    private BigDecimal quota;// 抵扣金额的消费额度
    private BigDecimal decrease;// 抵扣金额
    private Integer counting;// 次数
    private BigDecimal discount;// 折扣

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getArchiveId() {
        return archiveId;
    }

    public void setArchiveId(String archiveId) {
        this.archiveId = archiveId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getEffectiveFromTime() {
        return effectiveFromTime;
    }

    public void setEffectiveFromTime(LocalDateTime effectiveFromTime) {
        this.effectiveFromTime = effectiveFromTime;
    }

    public LocalDateTime getEffectiveToTime() {
        return effectiveToTime;
    }

    public void setEffectiveToTime(LocalDateTime effectiveToTime) {
        this.effectiveToTime = effectiveToTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public boolean isMember() {
        return isMember;
    }

    public void setMember(boolean member) {
        isMember = member;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getSendTime() {
        return sendTime;
    }

    public void setSendTime(LocalDateTime sendTime) {
        this.sendTime = sendTime;
    }

    public LocalDateTime getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(LocalDateTime receiveTime) {
        this.receiveTime = receiveTime;
    }

    public LocalDateTime getConsumeTime() {
        return consumeTime;
    }

    public void setConsumeTime(LocalDateTime consumeTime) {
        this.consumeTime = consumeTime;
    }

    public LocalDateTime getCancelTime() {
        return cancelTime;
    }

    public void setCancelTime(LocalDateTime cancelTime) {
        this.cancelTime = cancelTime;
    }

    public LocalDateTime getBackTime() {
        return backTime;
    }

    public void setBackTime(LocalDateTime backTime) {
        this.backTime = backTime;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getQuota() {
        return quota;
    }

    public void setQuota(BigDecimal quota) {
        this.quota = quota;
    }

    public BigDecimal getDecrease() {
        return decrease;
    }

    public void setDecrease(BigDecimal decrease) {
        this.decrease = decrease;
    }

    public Integer getCounting() {
        return counting;
    }

    public void setCounting(Integer counting) {
        this.counting = counting;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }
}