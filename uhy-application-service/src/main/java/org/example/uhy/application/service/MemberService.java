package org.example.uhy.application.service;

import org.example.uhy.application.context.ServiceContext;
import org.example.uhy.application.dto.MemberInfo;
import org.example.uhy.application.dto.MemberQueryCondition;
import org.example.uhy.application.dto.RegisterMemberDto;
import org.example.uhy.domain.model.member.MemberId;
import org.example.uhy.supports.meta.ApplicationService;

import java.util.List;

@ApplicationService
public interface MemberService {
    List<MemberInfo> members(MemberQueryCondition condition, ServiceContext context);

    MemberInfo memberOfId(String memberId, ServiceContext context);

    MemberId register(RegisterMemberDto memberDto, ServiceContext context);

    void calculateGrade(String memberId, ServiceContext context);

    void mark(String memberId, String label, ServiceContext context);

    void unMark(String memberId, ServiceContext context);
}