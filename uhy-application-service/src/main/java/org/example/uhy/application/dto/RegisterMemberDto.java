package org.example.uhy.application.dto;

import org.example.uhy.supports.meta.DTO;

@DTO
public class RegisterMemberDto {
    private String name;
    private String phone;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}