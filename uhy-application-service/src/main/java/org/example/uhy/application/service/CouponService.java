package org.example.uhy.application.service;

import org.example.uhy.application.context.ServiceContext;
import org.example.uhy.application.dto.CouponArchiveInfo;
import org.example.uhy.application.dto.CouponInfo;
import org.example.uhy.application.dto.CreateCouponArchiveDto;
import org.example.uhy.application.dto.MemberQueryCondition;
import org.example.uhy.supports.meta.ApplicationService;

import java.util.List;

/**
 * 卡券应用层服务，提供粗粒度服务接口，并且处理事务
 */
@ApplicationService
public interface CouponService {
    /**
     * 根据会员标签发券
     *
     * @param archiveId  卡券定义ID
     * @param operatorId 发券人
     * @param condition  会员匹配条件：标签、级别
     * @param context
     * @return 卡券ID集合
     */
    List<String> giveCouponsByCondition(String archiveId, String operatorId, MemberQueryCondition condition, ServiceContext context);

    /**
     * 人工或自动发放卡券场景
     *
     * @param archiveId  卡券定义ID
     * @param operatorId 发券人
     * @param memberIds  会员ID集合
     * @param context
     * @return 卡券ID集合
     */
    List<String> giveCouponsToMembers(String archiveId, String operatorId, List<String> memberIds, ServiceContext context);

    /**
     * 人工或自动发放卡券场景
     *
     * @param archiveId  卡券定义ID
     * @param operatorId 发券人
     * @param fansIds    粉丝ID集合
     * @param context
     * @return 卡券ID集合
     */
    List<String> giveCouponsToFans(String archiveId, String operatorId, List<String> fansIds, ServiceContext context);

    /**
     * 主动领取卡券场景
     *
     * @param archiveId  卡券定义ID
     * @param activityId 营销活动ID
     * @param memberId   会员ID
     * @param context
     * @return 卡券ID
     */
    String receiveCouponByMember(String archiveId, String activityId, String memberId, ServiceContext context);

    /**
     * 主动领取卡券场景
     *
     * @param archiveId  卡券定义ID
     * @param activityId 营销活动ID
     * @param fansId     粉丝ID
     * @param context
     * @return 卡券ID
     */
    String receiveCouponByFans(String archiveId, String activityId, String fansId, ServiceContext context);


    /**
     * 奖励卡券场景
     *
     * @param archiveId  卡券定义ID
     * @param activityId 营销活动ID
     * @param memberId   会员ID
     * @param context
     * @return 卡券ID
     */
    String awardCoupon(String archiveId, String activityId, String memberId, ServiceContext context);

    /**
     * 购买卡券场景
     *
     * @param archiveId 卡券定义ID
     * @param memberId  会员ID
     * @param context
     * @return 卡券ID
     */
    String buyCoupon(String archiveId, String orderId, String memberId, ServiceContext context);

    /**
     * 消费卡券
     *
     * @param couponId 卡券ID
     * @param memberId 会员ID
     * @param context
     */
    void consumeCoupon(String couponId, String memberId, ServiceContext context);

    /**
     * 废弃卡券
     *
     * @param couponId 卡券ID
     * @param memberId 会员ID
     * @param context
     */
    void cancelCoupon(String couponId, String tradeId, String memberId, ServiceContext context);

    /**
     * 扫描可用的卡券，由初始转换成可用
     *
     * @param context
     */
    void scheduleAvailableCoupons(ServiceContext context);

    /**
     * 扫描过期的卡券，由初始或可用转换成过期
     *
     * @param context
     */
    void scheduleExpiredCoupons(ServiceContext context);

    /**
     * 根据ID查找卡券
     *
     * @param couponId      卡券ID
     * @param context
     * @return
     */
    CouponInfo couponOfId(String couponId, ServiceContext context);

    /**
     * 创建卡券定义
     *
     * @param archiveDto 卡券定义
     * @param operatorId 卡券创建人
     * @param context
     * @return 卡券定义ID
     */
    String addCouponArchive(CreateCouponArchiveDto archiveDto, String operatorId, ServiceContext context);

    /**
     * 根据ID查找卡券定义
     *
     * @param archiveId 卡券定义ID
     * @param context
     * @return
     */
    CouponArchiveInfo couponArchiveOfId(String archiveId, ServiceContext context);
}