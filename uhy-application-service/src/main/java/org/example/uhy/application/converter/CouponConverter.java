package org.example.uhy.application.converter;

import org.example.uhy.application.dto.CouponArchiveInfo;
import org.example.uhy.application.dto.CouponInfo;
import org.example.uhy.domain.model.coupon.Coupon;
import org.example.uhy.domain.model.coupon.CouponArchive;
import org.springframework.stereotype.Component;

@Component
public class CouponConverter {
    public CouponArchiveInfo convert(CouponArchive archive) {
        if (archive == null) {
            return null;
        }
        CouponArchiveInfo archiveInfo = new CouponArchiveInfo();
        // 基本信息
        archiveInfo.setId(archive.archiveId().getId());
        archiveInfo.setName(archive.getName());
        archiveInfo.setDescription(archive.getDescription());
        // 有效期
        archiveInfo.setEffectiveFromTime(archive.effectiveTime().getFromTime());
        archiveInfo.setEffectiveToTime(archive.effectiveTime().getToTime());
        // 卡券发放约束条件定义
        archiveInfo.setScope(archive.constraint().getScope()); // 适用范围
        archiveInfo.setCountPerOwner(archive.constraint().getCountPerOwner()); // 每人限领数量
        // 卡券参数
        archiveInfo.setType(archive.parameter().getType());
        archiveInfo.setQuota(archive.parameter().getQuota());
        archiveInfo.setDecrease(archive.parameter().getDecrease());
        archiveInfo.setCounting(archive.parameter().getCounting());
        archiveInfo.setDiscount(archive.parameter().getDiscount());
        // 卡券库存
        archiveInfo.setPlannedQuantity(archive.stock().getPlannedQuantity()); // 预计发放数量
        archiveInfo.setQuantity(archive.stock().getQuantity()); // 可用数量，相当于库存
        return archiveInfo;
    }

    public CouponInfo convert(Coupon coupon) {
        if (coupon == null) {
            return null;
        }
        CouponInfo couponInfo = new CouponInfo();
        couponInfo.setId(coupon.couponId().getId());
        couponInfo.setArchiveId(coupon.archiveId().getId());
        couponInfo.setName(coupon.getName());
        // 有效期
        couponInfo.setEffectiveFromTime(coupon.effectiveTime().getFromTime());
        couponInfo.setEffectiveToTime(coupon.effectiveTime().getToTime());
        // 状态
        couponInfo.setStatus(coupon.currentStatus().ordinal());
        // 发券人
        if (coupon.hasOperator()) {
            couponInfo.setOperatorId(coupon.operator().getOperatorId());
        }
        // 所有人
        couponInfo.setOwnerId(coupon.owner().getOwnerId());
        couponInfo.setMember(coupon.owner().isMember());
        // 操作时间
        couponInfo.setSendTime(coupon.getSendTime());
        couponInfo.setReceiveTime(coupon.getReceiveTime());
        couponInfo.setConsumeTime(coupon.getConsumeTime());
        couponInfo.setCancelTime(coupon.getCancelTime());
        couponInfo.setBackTime(coupon.getBackTime());
        // 来源信息
        if (coupon.hasSource()) {
            couponInfo.setSourceId(coupon.sourceId().getSourceId());
            couponInfo.setSourceType(coupon.sourceId().getType());
        }
        // 卡券参数
        couponInfo.setType(coupon.parameter().getType()); // 卡券类型
        couponInfo.setQuota(coupon.parameter().getQuota());// 抵扣金额的消费额度
        couponInfo.setDecrease(coupon.parameter().getDecrease());// 抵扣金额
        couponInfo.setDiscount(coupon.parameter().getDiscount());// 折扣
        couponInfo.setCounting(coupon.parameter().getCounting());// 次数
        return couponInfo;
    }
}
