package org.example.uhy.application.dto;

import org.example.uhy.supports.meta.DTO;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 卡券定义信息
 */
@DTO
public class CouponArchiveInfo {
    // 基本信息
    private String id; // ID
    private String name; // 卡券类型名称
    private String description;
    // 有效期
    private LocalDateTime effectiveFromTime;
    private LocalDateTime effectiveToTime;
    // 卡券发放约束条件定义
    private String scope; // 适用范围
    private Integer countPerOwner; // 每人限领数量
    // 卡券参数
    private String type; // 卡券类型
    private BigDecimal quota;// 抵扣金额的消费额度
    private BigDecimal decrease;// 抵扣金额
    private Integer counting;// 次数
    private BigDecimal discount;// 折扣
    // 卡券库存
    private Integer plannedQuantity; // 预计发放数量，相当于库存总量
    private Integer quantity; // 可用数量，相当于库存现存量

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getEffectiveFromTime() {
        return effectiveFromTime;
    }

    public void setEffectiveFromTime(LocalDateTime effectiveFromTime) {
        this.effectiveFromTime = effectiveFromTime;
    }

    public LocalDateTime getEffectiveToTime() {
        return effectiveToTime;
    }

    public void setEffectiveToTime(LocalDateTime effectiveToTime) {
        this.effectiveToTime = effectiveToTime;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public Integer getCountPerOwner() {
        return countPerOwner;
    }

    public void setCountPerOwner(Integer countPerOwner) {
        this.countPerOwner = countPerOwner;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getQuota() {
        return quota;
    }

    public void setQuota(BigDecimal quota) {
        this.quota = quota;
    }

    public BigDecimal getDecrease() {
        return decrease;
    }

    public void setDecrease(BigDecimal decrease) {
        this.decrease = decrease;
    }

    public Integer getCounting() {
        return counting;
    }

    public void setCounting(Integer counting) {
        this.counting = counting;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public Integer getPlannedQuantity() {
        return plannedQuantity;
    }

    public void setPlannedQuantity(Integer plannedQuantity) {
        this.plannedQuantity = plannedQuantity;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
