package org.example.uhy.application.dto;

import org.example.uhy.domain.model.base.TimeRange;
import org.example.uhy.supports.meta.DTO;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@DTO
public class CreateCouponArchiveDto {
    // 基本信息
    private String name; // 卡券类型名称
    private String description;
    private String type; // 映射到具体的类，例如：DiscountCoupon、VoucherCoupon、TimesCoupon
    // 卡券参数
    private BigDecimal quota;// 抵扣金额的消费额度
    private BigDecimal decrease; // 抵扣金额
    private Integer counting;  // 次数
    private BigDecimal discount; // 折扣
    // 有效期
    private LocalDateTime effectiveFromTime;
    private LocalDateTime effectiveToTime;
    private transient TimeRange effectiveTime; // 有效期
    // 卡券限制条件
    private String scope; // 适用范围
    private Integer countPerOwner; // 每人限领数量
    // 卡券库存
    private Integer plannedQuantity; // 预计发放数量

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getQuota() {
        return quota;
    }

    public void setQuota(BigDecimal quota) {
        this.quota = quota;
    }

    public BigDecimal getDecrease() {
        return decrease;
    }

    public void setDecrease(BigDecimal decrease) {
        this.decrease = decrease;
    }

    public Integer getCounting() {
        return counting;
    }

    public void setCounting(Integer counting) {
        this.counting = counting;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public LocalDateTime getEffectiveFromTime() {
        return effectiveFromTime;
    }

    public void setEffectiveFromTime(LocalDateTime effectiveFromTime) {
        this.effectiveFromTime = effectiveFromTime;
    }

    public LocalDateTime getEffectiveToTime() {
        return effectiveToTime;
    }

    public void setEffectiveToTime(LocalDateTime effectiveToTime) {
        this.effectiveToTime = effectiveToTime;
    }

    public TimeRange getEffectiveTime() {
        return effectiveTime;
    }

    public void setEffectiveTime(TimeRange effectiveTime) {
        this.effectiveTime = effectiveTime;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public Integer getCountPerOwner() {
        return countPerOwner;
    }

    public void setCountPerOwner(Integer countPerOwner) {
        this.countPerOwner = countPerOwner;
    }

    public Integer getPlannedQuantity() {
        return plannedQuantity;
    }

    public void setPlannedQuantity(Integer plannedQuantity) {
        this.plannedQuantity = plannedQuantity;
    }
}
