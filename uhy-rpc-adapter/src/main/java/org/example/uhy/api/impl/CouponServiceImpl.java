package org.example.uhy.api.impl;

import org.apache.dubbo.config.annotation.DubboService;
import org.apache.dubbo.rpc.RpcContext;
import org.example.uhy.application.context.ServiceContext;
import org.example.uhy.application.service.CouponService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Dubbo 服务器端实现
 */
@DubboService
public class CouponServiceImpl implements org.example.uhy.api.CouponService {
    @Autowired
    CouponService couponService;

    @Override
    public boolean givingCoupons(String archiveId, List<String> ownerIds) {
        ServiceContext context = createServiceContext(RpcContext.getContext());
        couponService.giveCouponsToMembers(archiveId, context.currentOperator(), ownerIds, context);
        return true;
    }

    @Override
    public boolean receiveCoupon(String archiveId, String activityId) {
        ServiceContext context = createServiceContext(RpcContext.getContext());
        couponService.receiveCouponByMember(archiveId, activityId, context.currentOwner(), context);
        return true;
    }

    private ServiceContext createServiceContext(RpcContext context) {
        // 设置上下文信息
        return ServiceContext.robot();
    }
}
