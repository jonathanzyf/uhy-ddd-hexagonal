package org.example.uhy.repository.impl;

import org.example.uhy.domain.model.member.MemberLevel;
import org.example.uhy.domain.model.member.MemberLevelId;
import org.example.uhy.domain.model.member.MemberLevelRepository;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * 会员仓库实现类
 * 注意，这里是 {@link org.springframework.stereotype.Repository}
 */
@Repository
public class MemberLevelRepositoryImpl implements MemberLevelRepository {
    Map<MemberLevelId, MemberLevel> data = new HashMap<>();

    {
        MemberLevel levelOne = new MemberLevel(MemberLevelId.of("level-one"), "级别一");
        data.put(levelOne.levelId(), levelOne);

        MemberLevel levelTwo = new MemberLevel(MemberLevelId.of("level-two"), "级别二");
        data.put(levelTwo.levelId(), levelTwo);

        MemberLevel levelThree = new MemberLevel(MemberLevelId.of("level-three"), "级别三");
        data.put(levelThree.levelId(), levelThree);
    }

    @Override
    public MemberLevel defaultMemberLevel() {
        return new MemberLevel(MemberLevelId.of("level-one"), "级别一");
    }

    @Override
    public Optional<MemberLevel> memberLevelOfId(MemberLevelId levelId) {
        MemberLevel lvl = data.get(levelId);
        return Optional.ofNullable(lvl);
    }
}
