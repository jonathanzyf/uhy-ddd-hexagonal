package org.example.uhy.repository.impl;

import org.example.uhy.domain.model.member.*;
import org.example.uhy.repository.ram.RamStore;
import org.example.uhy.supports.mechanism.IdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * 会员仓库实现类
 * 注意，这里是 {@link org.springframework.stereotype.Repository}
 */
@Repository
public class MemberRepositoryImpl implements MemberRepository {
    @Autowired
    IdGenerator idGenerator;
    RamStore<MemberId, Member> store = new RamStore<>(10, 100);

    {
        MemberLabel label = MemberLabel.of("高富帅");
        Member mock001 = new Member(MemberId.of("会员-1949"), "1949", MemberLevelId.of("Level-one"), Phone.of("18901721949"));
        mock001.mark(label);
        store.put(mock001.memberId(), mock001);

        Member mock002 = new Member(MemberId.of("会员-9527"), "9527", MemberLevelId.of("Level-one"), Phone.of("18901729527"));
        mock002.mark(label);
        store.put(mock002.memberId(), mock002);
    }

    @Override
    public Optional<Member> memberOfId(MemberId memberId) {
        return store.get(memberId);
    }

    @Override
    public List<Member> membersOfLabel(MemberLabel label) {
        return store.filter(member -> member.hasLabel() && member.label().get().equals(label));
    }

    @Override
    public List<Member> membersOfLevel(MemberLevelId levelId) {
        return store.filter(member -> member.levelId().equals(levelId));
    }

    @Override
    public List<Member> members(MemberLabel label, MemberLevelId levelId) {
        return store.filter(member -> member.hasLabel() && member.label().get().equals(label) && member.levelId().equals(levelId));
    }

    @Override
    public void add(Member member) {
        store.put(member.memberId(), member);
    }

    @Override
    public void save(Member member) {
        store.put(member.memberId(), member);
    }

    @Override
    public MemberId nextIdentity() {
        return MemberId.of(idGenerator.nextIdentity());
    }
}
