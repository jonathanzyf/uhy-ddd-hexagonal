package org.example.uhy.repository.ram;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class RamStore<K, V> {
    private Cache<K, V> cache;

    public RamStore(int initialCapacity, int maximumSize) {
        cache = Caffeine.newBuilder()
//        软引用：如果一个对象只具有软引用，则内存空间足够，垃圾回收器就不会回收它；如果内存空间不足了，就会回收这些对象的内存。
//        弱引用：弱引用的对象拥有更短暂的生命周期。在垃圾回收器线程扫描它所管辖的内存区域的过程中，一旦发现了只具有弱引用的对象，不管当前内存空间足够与否，都会回收它的内存
                //.weakKeys()
                //.weakValues()
                .softValues()
                .initialCapacity(initialCapacity)//初始大小
                .maximumSize(maximumSize)//最大数量
                .removalListener((k, v, cause) -> {
                    // logger.info("remote key:" + k + " cause: " + cause.toString());
                })
                // .expireAfterWrite(1, TimeUnit.HOURS)//过期时间
                // .expireAfterAccess(cacheProperties.getExpireAfterAccessHours(), TimeUnit.HOURS)
                // .expireAfter()
                // .refreshAfterWrite(cacheProperties.getRefreshAfterWriteHours(), TimeUnit.HOURS)
                // .recordStats()
                .build();
    }

    public void put(K k, V v) {
        cache.put(k, v);
    }

    public void evict(K k) {
        cache.invalidate(k);
    }

    public Optional<V> get(K k) {
        V v = cache.getIfPresent(k);
        return Optional.ofNullable(v);
    }

    public List<V> filter(Predicate<V> filter) {
        Map<K, V> data = cache.asMap();
        return data.values().stream().filter(filter).collect(Collectors.toList());
    }
}
