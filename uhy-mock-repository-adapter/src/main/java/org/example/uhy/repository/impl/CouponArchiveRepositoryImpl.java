package org.example.uhy.repository.impl;

import org.example.uhy.domain.model.coupon.CouponArchive;
import org.example.uhy.domain.model.coupon.CouponArchiveId;
import org.example.uhy.domain.model.coupon.CouponArchiveRepository;
import org.example.uhy.repository.ram.RamStore;
import org.example.uhy.supports.mechanism.IdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * 卡券定义档案仓库实现类
 * 注意，这里是 {@link org.springframework.stereotype.Repository}
 */
@Repository
public class CouponArchiveRepositoryImpl implements CouponArchiveRepository {
    @Autowired
    IdGenerator idGenerator;
    RamStore<CouponArchiveId, CouponArchive> store = new RamStore<>(10, 100);

    @Override
    public void add(CouponArchive definition) {
        store.put(definition.archiveId(), definition);
    }

    @Override
    public Optional<CouponArchive> couponArchiveOfId(CouponArchiveId couponArchiveId) {
        return store.get(couponArchiveId);
    }

    @Override
    public void save(CouponArchive definition) {
        store.put(definition.archiveId(), definition);
    }

    @Override
    public CouponArchiveId nextIdentity() {
        return CouponArchiveId.of(idGenerator.nextIdentity());
    }
}
