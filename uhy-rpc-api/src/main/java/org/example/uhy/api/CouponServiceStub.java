package org.example.uhy.api;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 对外发布的Dubbo Api 存根，在客户端本地进行校验
 */
public class CouponServiceStub implements CouponService {
    private CouponService proxy;

    private CouponServiceStub() {
    }

    public CouponServiceStub(CouponService proxy) {
        this.proxy = proxy;
    }


    @Override
    public boolean givingCoupons(String couponArchiveId, List<String> ownerIds) {
        checkCouponArchiveId(couponArchiveId);
        return proxy.givingCoupons(couponArchiveId, filterOwnerId(ownerIds));
    }

    @Override
    public boolean receiveCoupon(String archiveId, String activityId) {
        checkCouponArchiveId(archiveId);
        return proxy.receiveCoupon(archiveId, activityId);
    }

    private void checkCouponArchiveId(String couponArchiveId) {
        if (couponArchiveId == null || couponArchiveId.length() == 0) {
            throw new RuntimeException("请传入卡券定义档案ID");
        }
    }

    private List<String> filterOwnerId(List<String> ownerIds) {
        if (ownerIds == null || ownerIds.size() == 0) {
            throw new RuntimeException("请传入卡券所属人");
        }
        return ownerIds.stream().filter(ownerId -> ownerId != null && ownerId.length() > 0).collect(Collectors.toList());
    }
}
