package org.example.uhy.api;

import java.util.List;

/**
 * 对外发布的Dubbo Api
 */
public interface CouponService {
    boolean givingCoupons(String archiveId, List<String> ownerIds);

    boolean receiveCoupon(String archiveId, String activityId);
}